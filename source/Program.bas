Attribute VB_Name = "Program"
Option Explicit
Option Compare Text
'  Copyright (C) 2000-2004, Julien Lecomte
'
'  This software Is provided 'as-is', without any express or implied
'  warranty.  In no event will the authors be held liable for any damages
'  arising from the use of this software.
'
'  Permission is granted to anyone to use this software for any purpose,
'  including commercial applications, and to alter it and redistribute it
'  freely, subject to the following restrictions:
'
'  1. The origin of this software must not be misrepresented; you must not
'     claim that you wrote the original software. If you use this software
'     in a product, an acknowledgment in the product documentation would be
'     appreciated but is not required.
'  2. Altered source versions must be plainly marked as such, and must not be
'     misrepresented as being the original software.
'  3. This notice may not be removed or altered from any source distribution.
'

'/C /W "f:\copied~1\crad?e*.mp3" /0 "titre" /0 "artist" /0 "album" /1 "year" /2 "comment" /3 ""

'************************************************************************************
Public Type mp3Tag
    sTag     As String * 3
    sTitle   As String * 30
    sArtist  As String * 30
    sAlbum   As String * 30
    sYear    As String * 4
    sComment As String * 28
    sTrack   As String * 2
    bGenre   As Byte
End Type

Public Const saGenreLimit As Byte = 147
Public Const sRegistryTagPro = "Software\Freeware\TagPro"
Public Const sRegistryOptions = "Software\Freeware\TagPro\Options"
Public Const sRegistryBatch = "Software\Freeware\TagPro\Batch"
Public Const sRegistryWindow = "Software\Freeware\TagPro\Window"

Public lProcOld           As Long
Public bOptionSeeCaption  As Boolean
Public saGenres(0 To 255) As String
Public tMemTag            As mp3Tag

Public Function AddSlash(ByVal sString As String) As String
    AddSlash = sString & IIf(Right$(sString, 1) = "\", "", "\")
End Function

Public Function Command_Act(ByVal sCommand As String) As Long
    'Command Line
    '/C [/W] "%path%"
    '/- | [/M] /x [""]*
    '===================================================================
    '/C : Command line
    '/W : Don't warn if error.
    '"%": Path of file. Wildcards *,? accepted. Must be .mp3
    '/- : Delete
    '                              Exist | Not
    '/x : x takes value of | 0 = Nothing | Nothing
    '                      | 1 = Replace | Nothing (or create if blank)
    '                      | 2 = Nothing | Replace (or create if blank)
    '                      | 3 = Replace | Replace (or create if blank)
    'In order:
    'sTitle   $30
    'sArtist  $30
    'sAlbum   $30
    'sYear    $4
    'sComment $30
    'bGenre   0-
    
    Dim lRetVal&
    Dim I&, J&
    Dim iError%, iPlace%
    Dim sDirectory$
    Dim sFilePath$, sFileDir$
    Dim sBuffer$
    Dim sTitle$, sArtist$
    Dim bWarn    As Boolean
    Dim bInside  As Boolean
    Dim bDelete  As Boolean
    Dim bTest    As Boolean
    Dim bMake    As Boolean
    Dim bTag(1 To 6) As Byte
    Dim sTag(1 To 6) As String
    Dim tFile    As mp3Tag
    Dim tWrite   As mp3Tag
    Dim tDummy   As mp3Tag
    Dim sFiles() As String
    Dim sTmpRestore As String
    
On Error GoTo LaunchError
    'Initialize
    bWarn = True
    tDummy.sTag = "TAG"
    tDummy.bGenre = 255
    tDummy.sAlbum = ""
    tDummy.sArtist = ""
    tDummy.sYear = ""
    tDummy.sComment = ""
    tDummy.sTitle = ""

    'Start
    If Left$(UCase$(sCommand), 1) = "I" Then
        bInside = True
        bWarn = False
        sCommand = LTrim$(Mid$(sCommand, 2)) 'Cut the I
    End If
    
    sCommand = LTrim$(Mid$(sCommand, 3)) 'Cut the /C
    
    If Left$(UCase$(sCommand), 2) = "/W" Then
        bWarn = False
        sCommand = LTrim$(Mid$(sCommand, 3)) 'Cut the /W
    End If
        
    If Not (Left$(UCase$(sCommand), 1) = """") Then
        iError = 1
        GoTo CommandError
    End If
    sCommand = LTrim$(Mid$(sCommand, 2)) 'Cut the first '"'
    
    iPlace = InStr(1, sCommand, """", vbBinaryCompare)
    If iPlace <= 1 Then
        iError = 1
        GoTo CommandError
    End If
    sFilePath = Mid$(sCommand, 1, iPlace - 1)
    sCommand = LTrim$(Mid$(sCommand, iPlace + 1))
    
    'Get long path if it is not long path
    sFileDir = sFilePath
    sTmpRestore = sFilePath
    sBuffer = Space$(255&)
    lRetVal = GetLongPathName(sFilePath, sBuffer, 255&)
    sFilePath = Left$(sBuffer, lRetVal)
    If UCase$(Right$(sFilePath, 4)) <> ".MP3" Then
        iError = 2
        GoTo CommandError
IgnoreErrorQuickFix:
        sFilePath = sTmpRestore
        
    End If
        
    'get extras!
    If Left$(UCase$(sCommand), 2) = "/-" Then
        bDelete = True
        sCommand = LTrim$(Mid$(sCommand, 3)) 'Cut the /-
    Else
        If Left$(UCase$(sCommand), 2) = "/M" Then
            bMake = True
            sCommand = LTrim$(Mid$(sCommand, 3)) 'Cut the /M
        End If
    
        I = 1
        Do
            bTest = True
            If Left$(sCommand, 2) Like "/[0-3]" Then
                bTest = False
                bTag(I) = CByte(Mid$(sCommand, 2, 1))
                sCommand = LTrim$(Mid$(sCommand, 3)) 'Cut the /#
                If Not (Left$(UCase$(sCommand), 1) = """") Then
                    iError = 2 + I
                    GoTo CommandError
                End If
                sCommand = LTrim$(Mid$(sCommand, 2)) 'Cut the '"'
                iPlace = InStr(1, sCommand, """", vbBinaryCompare)
                If iPlace = 0 Then
                    iError = 2 + I
                    GoTo CommandError
                End If
                sTag(I) = Mid$(sCommand, 1, iPlace - 1)
                sCommand = LTrim$(Mid$(sCommand, iPlace + 1))
            ElseIf Len(sCommand) <> 0 Then
                iError = 2 + I
                GoTo CommandError
            End If
            I = I + 1
        Loop Until (I = 7) Or bTest
    End If
        
    '***********************
    'Get directory
    sDirectory = Get_Directory(sFileDir)
    
    'Open all files in path.
    'Fill an array of accurate paths.
    ReDim sFiles(1 To 1) As String
    sBuffer = Dir$(sFileDir)
    lRetVal = GetAttr(sDirectory & sBuffer)
    'find first not readonly file
    If (Not CBool(lRetVal And vbReadOnly)) Then
        sFiles(1) = sBuffer
    Else
        Do While sBuffer <> ""
            sBuffer = Dir
            If sBuffer <> "" Then
                lRetVal = GetAttr(sDirectory & sBuffer)
                If (Not CBool(lRetVal And vbReadOnly)) Then
                    sFiles(1) = sBuffer
                End If
            End If
        Loop
    End If
    
    Do While sBuffer <> ""
        sBuffer = Dir
        If sBuffer <> "" Then
            lRetVal = GetAttr(sDirectory & sBuffer)
            If (Not CBool(lRetVal And vbReadOnly)) Then
                ReDim Preserve sFiles(1 To UBound(sFiles) + 1) As String
                sFiles(UBound(sFiles)) = sBuffer
            End If
        End If
    Loop
    'if empty then error no file!
    If sFiles(1) = "" Then
        iError = 9
        GoTo CommandError
    End If
    
    'Don't sort the array, it's useless.
    '***********************
    'ACTION
    'Loop through all!
    For I = 1 To UBound(sFiles)
        lRetVal = File_ReadTag(sDirectory & sFiles(I), tFile)
        If lRetVal Then
            iError = -1
            GoTo CommandError
        End If
        
        Select Case bDelete
            Case True
                If tFile.sTag = "TAG" Then File_Truncate sDirectory & sFiles(I)
            Case False
                'Prepare tag
                If tFile.sTag = "TAG" Then
                    tWrite = tFile
                Else
                    tWrite = tDummy
                End If
                
                If bMake Then
                    If File_MakeTag(Left$(sFiles(I), Len(sFiles(I)) - 4), sTitle, sArtist) Then
                        sTag(1) = sTitle
                        sTag(2) = sArtist
                    End If
                End If
                
                For J = 1 To 6
                    If bTag(J) = 3 Or _
                       bTag(J) = 2 And tFile.sTag <> "TAG" Or _
                       bTag(J) = 1 And tFile.sTag = "TAG" Then
                        Select Case J
                            Case 1
                                tWrite.sTitle = Left$(sTag(1), 30)
                            Case 2
                                tWrite.sArtist = Left$(sTag(2), 30)
                            Case 3
                                tWrite.sAlbum = Left$(sTag(3), 30)
                            Case 4
                                tWrite.sYear = Left$(sTag(4), 4)
                            Case 5
                                tWrite.sComment = Left$(sTag(5), 30)
                            Case 6
                                sTag(6) = Trim$(sTag(6))
                                If CStr(Val(Left$(sTag(6), 3))) <> Left$(sTag(6), 3) Then _
                                    sTag(6) = "255"
                                lRetVal = Val(Left$(sTag(6), 3))
                                If lRetVal < 0 Or lRetVal > 255 Then lRetVal = 255
                                tWrite.bGenre = CByte(lRetVal)
                        End Select
                    End If
                Next
                File_SaveTag sDirectory & sFiles(I), tWrite, CBool(tFile.sTag = "TAG")
        End Select
    Next
    Exit Function
    
CommandError:
    Reset
    If bInside Then
        Command_Act = iError
        GoTo InsideError
    End If
    If Not bWarn Then Exit Function
    
InsideError:
On Error GoTo LaunchError
    Select Case iError
        Case -1 'Real error, lretval = err.number
            Err.Raise lRetVal
        Case 1 'Command line path error!
            MsgBox "Command line path error.", vbOKOnly, "TagPro Command line"
        Case 2 'File is not "*.mp3"
            MsgBox "No mp3 found. Some might be as read only. Error will be ignored", vbOKOnly, "TagPro Command line"
            GoTo IgnoreErrorQuickFix
        Case 3, 4, 5, 6, 7, 8 'Tag error
            MsgBox "Error in tag object n�" & iError - 2, vbOKOnly, "TagPro Command line"
        Case 9 'No suitable not readonly file
            MsgBox "No suitable files. Files must not be readonly.", vbOKOnly, "TagPro Command line"
    End Select
    Exit Function
LaunchError:
    Reset
    If Not bWarn Then Exit Function
    ErrHandler Err
End Function

Private Function Command_Analyze(sFileName As String, sDirectory As String) As Long
    Dim sCmd$
    Dim lAttr&
    Dim sBuffer$
    Dim lLen&
    
    sCmd = UCase$(Command$)
    sCmd = Trim$(sCmd)
    
    If Left$(sCmd, 2) = "/C" Then
        Command_Analyze = 3
        Exit Function
    End If
    
    'Drag and drop?
    'Get only the first file/directory
    If InStr(1, sCmd, " ", vbBinaryCompare) Then
        sCmd = Left$(sCmd, InStr(1, sCmd, " ", vbBinaryCompare))
        sCmd = Trim$(sCmd)
    End If
    
    'Does such exist?
    If Dir(sCmd, vbDirectory) = "" And Dir(sCmd) = "" Then Exit Function
    
    'Get the long file name
    sBuffer = Space(255&)
    lLen = GetLongPathName(sCmd, sBuffer, 255&)
    sCmd = UCase$(Left$(sBuffer, lLen))
    
    'File or directory?
    lAttr = GetAttr(sCmd)
    If lAttr And vbDirectory Then
        sDirectory = sCmd
        Command_Analyze = 2
        Exit Function
    Else
        'File: must be mp3
        If Right$(sCmd, 4) = ".MP3" Then
            sFileName = Get_FileTitle(sCmd)
            sDirectory = Left$(sCmd, Len(sCmd) - Len(sFileName))
            sFileName = Left$(sFileName, Len(sFileName) - 4)
            Command_Analyze = 1
            Exit Function
        Else
            Command_Analyze = -1
            Exit Function
        End If
    End If
End Function

Public Function ErrHandler(objError As ErrObject) As Long
    Dim Msg$
    Select Case objError.Number
        Case 7
            Msg = "Out of memory. Try closing unused applications or reboot."
        Case 53
            Msg = "File not found."
        Case 55
            Msg = "File already open."
        Case 57
            Msg = "Device I/O error."
        Case 61
            Msg = "Disk full."
        Case 63
            Msg = "File error."
        Case 67
            Msg = "Too many files opened by Windows."
        Case 68
            Msg = "Device unavailable."
        Case 70
            Msg = "Permission denied."
        Case 71
            Msg = "Disk not ready."
        Case 74
            Msg = "Can't rename with different drive."
        Case 75
            Msg = "Path/File access error."
        Case 76
            Msg = "Path not found."
        Case Else
            Msg = objError.Description & vbNewLine
            MsgBox Msg, vbCritical + vbOKOnly, "Error n�" & objError.Number
            ErrHandler = 1
            Exit Function
    End Select
    MsgBox Msg, vbCritical + vbOKOnly, "Error"
End Function

Private Function Get_Directory(ByVal sLongPath As String) As String
    Dim iRetVal%
    Dim sTmp$, sBuffer$
    
    sTmp = StrReverse(sLongPath)
    iRetVal = InStr(1, sTmp, "\", vbBinaryCompare)
    sTmp = Right$(sTmp, Len(sTmp) - iRetVal)
    sTmp = StrReverse(sTmp)
    
    sBuffer = Space(255&)
    iRetVal = GetLongPathName(sTmp, sBuffer, 255&)
    sTmp = Left$(sBuffer, iRetVal)
    
    Get_Directory = AddSlash(sTmp)
End Function

Public Function Get_FileTitle(sLongPath As String) As String
    Dim sBuffer$
    sBuffer = Space$(255&)
    
    GetFileTitle sLongPath, sBuffer, 255
    sBuffer = Left$(sBuffer, InStr(1, sBuffer, vbNullChar) - 1) & ".mp3"
    Get_FileTitle = sBuffer
End Function

Public Function GetOption(ByVal sKeyName As String, byRet As Byte, Optional byIfNoVal As Boolean) As Boolean
    Dim lRetVal&
    Dim vValue As Variant
    If Not byIfNoVal Then
        GetRegistry HKEY_CURRENT_USER, sRegistryOptions, sKeyName, vValue
    Else
        lRetVal = GetRegistry(HKEY_CURRENT_USER, sRegistryOptions, sKeyName, vValue)
        If lRetVal = REG_ERROR Then
            GetOption = True
            Exit Function
        End If
    End If
    
    lRetVal = vValue
    GetOption = CBool(lRetVal And 4)
    byRet = lRetVal - (lRetVal And 4)
    Select Case byRet
        Case 0 'Depends
        Case 1:  GetOption = True
        Case 2:  GetOption = False
    End Select
End Function

'************************************************************************************
Private Sub Main()
    Dim lAnalyzeCmd&
    Dim sDirectory$
    Dim sFileName$
    Tags
    
    If Command$ <> "" Then
        lAnalyzeCmd = Command_Analyze(sFileName, sDirectory)

        Select Case lAnalyzeCmd
            Case -1
                MsgBox "Not an mp3 file.", vbOKOnly + vbCritical, "Error"
            Case 0
                MsgBox "Error in command line.", vbOKOnly + vbCritical, "Error"
            Case 1
                'mp3 drag & drop or context menu
                'F:\Cradle~2.mp3 F:\Cradle~1.mp3
                Load frmMain
                frmMain.lbDrive = Left$(sDirectory, 3)
                frmMain.lbDirectory = sDirectory
                frmMain.lbFiles.ListIndex = SendMessage(frmMain.lbFiles.hwnd, LB_FINDSTRING, -1, ByVal sFileName)
            Case 2
                'directory drag & drop
                Load frmMain
                frmMain.lbDrive = Left$(sDirectory, 3)
                frmMain.lbDirectory = sDirectory
            Case 3
                Command_Act LTrim$(Command$)
                Exit Sub
        End Select
    End If
    frmMain.Show
End Sub

Public Function SetOption(ByVal sKeyName As String, boolVal As Boolean, Optional bGetVal As Boolean = True, Optional byteVal As Byte)
    Dim byteRet As Byte
    Dim lVal&
    If bGetVal Then
        GetOption sKeyName, byteRet
    Else
        byteRet = byteVal
    End If
    lVal = byteRet + IIf(boolVal, 4, 0)
    SetRegistry HKEY_CURRENT_USER, sRegistryOptions, sKeyName, lVal, REG_DWORD
End Function

Public Function Shell_BrowseFolder(hwndOwner As Long, sPrompt As String) As String
    Dim iNull%
    Dim lpIDList&
    Dim sPath$
    Dim udtBI As BrowseInfo
    
    With udtBI
        .hwndOwner = hwndOwner
        .lpszTitle = lstrcat(sPrompt, "")
        .ulFlags = BIF_RETURNONLYFSDIRS
    End With
    lpIDList = SHBrowseForFolder(udtBI)
    If lpIDList Then
         sPath = String$(MAX_PATH, 0)
         SHGetPathFromIDList lpIDList, sPath
         CoTaskMemFree lpIDList
         iNull = InStr(sPath, vbNullChar)
         If iNull Then sPath = Left$(sPath, iNull - 1)
    End If
    Shell_BrowseFolder = sPath
End Function

Public Function SysMenuHandler(ByVal hwnd As Long, ByVal iMsg As Long, ByVal wParam As Long, ByVal lParam As Long) As Long
    If iMsg = WM_SYSCOMMAND Then
        Select Case wParam
            Case IDM_ABOUT
                frmAbout.Show vbModal, frmMain
                Exit Function
            Case IDM_TRAY
                frmMain.Tray True
                Exit Function
        End Select
    End If
    SysMenuHandler = CallWindowProc(lProcOld, hwnd, iMsg, wParam, lParam)
End Function

Private Sub Tags()
    saGenres(123) = "A Capella"
    saGenres(34) = "Acid"
    saGenres(74) = "Acid Jazz"
    saGenres(73) = "Acid Punk"
    saGenres(99) = "Acoustic"
    saGenres(40) = "Alt.Rock"
    saGenres(20) = "Alternative"
    saGenres(26) = "Ambient"
    saGenres(145) = "Anime"
    saGenres(90) = "Avantgarde"
    saGenres(116) = "Ballad"
    saGenres(41) = "Bass"
    saGenres(135) = "Beat"
    saGenres(85) = "Bebob"
    saGenres(96) = "Big Band"
    saGenres(138) = "Black Metal"
    saGenres(89) = "Bluegrass"
    saGenres(0) = "Blues"
    saGenres(107) = "Booty Bass"
    saGenres(132) = "BritPop"
    saGenres(65) = "Cabaret"
    saGenres(88) = "Celtic"
    saGenres(104) = "Chamber Music"
    saGenres(102) = "Chanson"
    saGenres(97) = "Chorus"
    saGenres(136) = "Christian Gangsta"
    saGenres(61) = "Christian Rap"
    saGenres(141) = "Christian Rock"
    saGenres(1) = "Classic Rock"
    saGenres(32) = "Classical"
    saGenres(112) = "Club"
    saGenres(128) = "Club-House"
    saGenres(57) = "Comedy"
    saGenres(140) = "Contemporary Christian"
    saGenres(2) = "Country"
    saGenres(139) = "Crossover"
    saGenres(58) = "Cult"
    saGenres(3) = "Dance"
    saGenres(125) = "Dance Hall"
    saGenres(50) = "Darkwave"
    saGenres(22) = "Death Metal"
    saGenres(4) = "Disco"
    saGenres(55) = "Dream"
    saGenres(127) = "Drum & Bass"
    saGenres(122) = "Drum solo"
    saGenres(120) = "Duet"
    saGenres(98) = "Easy listnening"
    saGenres(52) = "Electronic"
    saGenres(48) = "Ethic"
    saGenres(54) = "Eurodance"
    saGenres(124) = "Euro-House"
    saGenres(25) = "Euro-Techno"
    saGenres(84) = "Fast-Fusion"
    saGenres(80) = "Folk"
    saGenres(81) = "Folk/Rock"
    saGenres(115) = "Folklore"
    saGenres(119) = "Freestyle"
    saGenres(5) = "Funk"
    saGenres(30) = "Fusion"
    saGenres(36) = "Game"
    saGenres(59) = "Gangsta Rap"
    saGenres(126) = "Goa"
    saGenres(38) = "Gospel"
    saGenres(49) = "Gothic"
    saGenres(91) = "Gothic Rock"
    saGenres(6) = "Grunge"
    saGenres(129) = "Hardcore"
    saGenres(79) = "Hard Rock"
    saGenres(137) = "Heavy Metal"
    saGenres(7) = "Hip-Hop"
    saGenres(35) = "House"
    saGenres(100) = "Humor"
    saGenres(131) = "Indie"
    saGenres(19) = "Industrial"
    saGenres(46) = "Industrial Pop"
    saGenres(47) = "Industrial Rock"
    saGenres(33) = "Instrumental"
    saGenres(8) = "Jazz"
    saGenres(29) = "Jazz+Funk"
    saGenres(146) = "JPop"
    saGenres(63) = "Jungle"
    saGenres(86) = "Latin"
    saGenres(71) = "Lo-Fi"
    saGenres(45) = "Meditative"
    saGenres(142) = "Merengue"
    saGenres(9) = "Metal"
    saGenres(77) = "Musical"
    saGenres(82) = "National Folk"
    saGenres(64) = "Native American"
    saGenres(133) = "Negerpunk"
    saGenres(10) = "New Age"
    saGenres(66) = "New Wave"
    saGenres(39) = "Noise"
    saGenres(11) = "Oldies"
    saGenres(103) = "Opera"
    saGenres(12) = "Other"
    saGenres(75) = "Polka"
    saGenres(134) = "Polsk Punk"
    saGenres(13) = "Pop"
    saGenres(62) = "Pop/Funk"
    saGenres(53) = "Pop-Folk"
    saGenres(109) = "Porn Groove"
    saGenres(117) = "Power Ballad"
    saGenres(23) = "Pranks"
    saGenres(108) = "Primus"
    saGenres(92) = "Progressive Rock"
    saGenres(67) = "Psychedelic"
    saGenres(93) = "Psychedelic Rock"
    saGenres(43) = "Punk"
    saGenres(121) = "Punk Rock"
    saGenres(14) = "R&B"
    saGenres(15) = "Rap"
    saGenres(68) = "Rave"
    saGenres(16) = "Reggae"
    saGenres(76) = "Retro"
    saGenres(87) = "Revival"
    saGenres(118) = "Rhythmic Soul"
    saGenres(17) = "Rock"
    saGenres(78) = "Rock & Roll"
    saGenres(143) = "Salsa"
    saGenres(114) = "Samba"
    saGenres(110) = "Satire"
    saGenres(69) = "Showtunes"
    saGenres(21) = "Ska"
    saGenres(111) = "Slow Jam"
    saGenres(95) = "Slow Rock"
    saGenres(105) = "Sonata"
    saGenres(42) = "Soul"
    saGenres(37) = "Sound Clip"
    saGenres(24) = "Soundtrack"
    saGenres(56) = "Southern Rock"
    saGenres(44) = "Space"
    saGenres(101) = "Speech"
    saGenres(83) = "Swing"
    saGenres(94) = "Symphonic Rock"
    saGenres(106) = "Symphony"
    saGenres(147) = "Synthpop"
    saGenres(113) = "Tango"
    saGenres(18) = "Techno"
    saGenres(51) = "Techno-Industrial"
    saGenres(130) = "Terror"
    saGenres(144) = "Thrash Metal"
    saGenres(60) = "Top 40"
    saGenres(70) = "Trailer"
    saGenres(31) = "Trance"
    saGenres(72) = "Tribal"
    saGenres(27) = "Trip-Hop"
    saGenres(28) = "Vocal"
End Sub
