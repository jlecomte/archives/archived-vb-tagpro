Attribute VB_Name = "Registry"
Option Explicit
Option Base 1
Option Compare Text
'  Copyright (C) 2000-2004, Julien Lecomte
'
'  This software Is provided 'as-is', without any express or implied
'  warranty.  In no event will the authors be held liable for any damages
'  arising from the use of this software.
'
'  Permission is granted to anyone to use this software for any purpose,
'  including commercial applications, and to alter it and redistribute it
'  freely, subject to the following restrictions:
'
'  1. The origin of this software must not be misrepresented; you must not
'     claim that you wrote the original software. If you use this software
'     in a product, an acknowledgment in the product documentation would be
'     appreciated but is not required.
'  2. Altered source versions must be plainly marked as such, and must not be
'     misrepresented as being the original software.
'  3. This notice may not be removed or altered from any source distribution.
'
Private Type SECURITY_ATTRIBUTES
    nLength              As Long
    lpSecurityDescriptor As Long
    bInheritHandle       As Long
End Type

Public Enum HKEY
    HKEY_CLASSES_ROOT = &H80000000
    HKEY_CURRENT_USER = &H80000001
    HKEY_LOCAL_MACHINE = &H80000002
End Enum

Public Enum REG
    REG_ERROR = 0&
    REG_SZ = 1&
    REG_BINARY = 3&
    REG_DWORD = 4&
End Enum

Private Const READ_CONTROL = &H20000

Private Const STANDARD_RIGHTS_ALL = &H1F0000

Private Const SYNCHRONIZE = &H100000

Private Const KEY_SET_VALUE = &H2&
Private Const KEY_QUERY_VALUE = &H1&
Private Const KEY_CREATE_SUB_KEY = &H4&
Private Const KEY_CREATE_LINK = &H20&
Private Const KEY_ENUMERATE_SUB_KEYS = &H8&
Private Const KEY_NOTIFY = &H10&

Private Const KEY_READ = ((READ_CONTROL Or KEY_QUERY_VALUE Or KEY_ENUMERATE_SUB_KEYS Or KEY_NOTIFY) And (Not SYNCHRONIZE))
Private Const KEY_WRITE = ((READ_CONTROL Or KEY_SET_VALUE Or KEY_CREATE_SUB_KEY) And (Not SYNCHRONIZE))

Private Declare Function RegCloseKey Lib "advapi32.dll" (ByVal HKEY As Long) As Long
Private Declare Function RegCreateKeyEx Lib "advapi32.dll" Alias "RegCreateKeyExA" (ByVal HKEY As Long, ByVal lpSubKey As String, ByVal Reserved As Long, ByVal lpClass As String, ByVal dwOptions As Long, ByVal samDesired As Long, lpSecurityAttributes As SECURITY_ATTRIBUTES, phkResult As Long, lpdwDisposition As Long) As Long
Private Declare Function RegSetValueEx Lib "advapi32.dll" Alias "RegSetValueExA" (ByVal HKEY As Long, ByVal lpValueName As String, ByVal Reserved As Long, ByVal dwType As Long, lpData As Any, ByVal cbData As Long) As Long
Private Declare Function RegOpenKeyEx Lib "advapi32.dll" Alias "RegOpenKeyExA" (ByVal HKEY As Long, ByVal lpSubKey As String, ByVal ulOptions As Long, ByVal samDesired As Long, phkResult As Long) As Long
Private Declare Function RegQueryValueEx Lib "advapi32.dll" Alias "RegQueryValueExA" (ByVal HKEY As Long, ByVal lpValueName As String, ByVal lpReserved As Long, lpType As Long, lpData As Any, lpcbData As Long) As Long

Public Function GetRegistry(lHKey As HKEY, sSubKey As String, sKeyName As String, vKeyValue As Variant, Optional vpDefaultValue As Variant) As Long
    Dim hVal&, RetVal&, I&
    Dim lType&, lSize&
    Dim sString$, lLong&
    Dim bData() As Byte
       
    RetVal = RegOpenKeyEx(lHKey, sSubKey, 0&, KEY_READ, hVal)
    If RetVal <> 0& Then
        If Not IsMissing(vpDefaultValue) Then vKeyValue = vpDefaultValue
        Exit Function
    Else
        RetVal = RegQueryValueEx(hVal, sKeyName, ByVal 0&, lType, ByVal 0&, lSize)
        If RetVal <> 0& Then
            If Not IsMissing(vpDefaultValue) Then vKeyValue = vpDefaultValue
            RegCloseKey hVal
            Exit Function
        End If
    End If
        
    Select Case lType
        Case REG_SZ
            ReDim bData(1 To lSize) As Byte
            RegQueryValueEx hVal, sKeyName, ByVal 0&, REG_BINARY, bData(1), lSize
            
            sString = ""
            
            For I = 1 To lSize - 1
                sString = sString & Chr(bData(I))
            Next
            
            vKeyValue = sString
            GetRegistry = REG_SZ

        Case REG_DWORD
            RegQueryValueEx hVal, sKeyName, ByVal 0&, REG_DWORD, lLong, 4&
            vKeyValue = lLong
            GetRegistry = REG_DWORD
        
        Case REG_BINARY, REG_SZ
            ReDim bData(1 To lSize) As Byte
            RegQueryValueEx hVal, sKeyName, ByVal 0&, REG_BINARY, bData(1), lSize
            vKeyValue = bData
            GetRegistry = REG_BINARY
        Case Else
            If Not IsMissing(vpDefaultValue) Then vKeyValue = vpDefaultValue
    End Select

    RegCloseKey hVal

End Function

Public Function SetRegistry(lHKey As HKEY, sSubKey As String, ByVal sKeyName As String, vKeyValue As Variant, RegType As Long) As Long
    Dim hVal&, lLong&, RetVal&
    Dim sString$
    Dim SA As SECURITY_ATTRIBUTES
    SA.nLength = Len(SA)
            
    RetVal = RegCreateKeyEx(lHKey, sSubKey, 0&, "", 0&, KEY_WRITE, SA, hVal, ByVal 0&)
    If RetVal <> 0& Then Exit Function
    Select Case RegType
        Case REG_SZ
            sString = CStr(vKeyValue) & vbNullChar
            RegSetValueEx hVal, sKeyName, 0&, REG_SZ, ByVal sString, Len(sString)
            SetRegistry = REG_SZ
        Case REG_BINARY
            RegSetValueEx hVal, sKeyName, 0&, REG_BINARY, vKeyValue, Len(vKeyValue)
            SetRegistry = REG_BINARY
        Case REG_DWORD
            lLong = CLng(vKeyValue)
            RegSetValueEx hVal, sKeyName, 0&, REG_DWORD, lLong, 4&
            SetRegistry = REG_DWORD
        Case Else
            Exit Function
    End Select
    RegCloseKey hVal
End Function
