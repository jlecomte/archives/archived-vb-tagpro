Attribute VB_Name = "Tag"
Option Explicit
'  Copyright (C) 2000-2004, Julien Lecomte
'
'  This software Is provided 'as-is', without any express or implied
'  warranty.  In no event will the authors be held liable for any damages
'  arising from the use of this software.
'
'  Permission is granted to anyone to use this software for any purpose,
'  including commercial applications, and to alter it and redistribute it
'  freely, subject to the following restrictions:
'
'  1. The origin of this software must not be misrepresented; you must not
'     claim that you wrote the original software. If you use this software
'     in a product, an acknowledgment in the product documentation would be
'     appreciated but is not required.
'  2. Altered source versions must be plainly marked as such, and must not be
'     misrepresented as being the original software.
'  3. This notice may not be removed or altered from any source distribution.
'
Public Function File_MakeTag(ByVal sName As String, sTitle As String, sArtist As String) As Boolean
    Dim sTmp$
    
    File_MakeTag = True
    If sName Like "*-*" Then
        sArtist = Left$(Trim$(Replace_MP3(Left$(sName, InStr(1, sName, "-") - 1))), 30)
        sTitle = Left$(Trim$(Replace_MP3(Mid$(sName, InStr(1, sName, "-") + 1))), 30)
    ElseIf sName Like "(*)*" Then
        sTmp = Mid$(sName, 2)
        sTmp = Left$(sTmp, InStr(1, sTmp, ")", vbBinaryCompare) - 1)
        sArtist = Trim$(Replace_MP3(sTmp))
        sTmp = Mid$(sName, InStr(1, sName, ")", vbBinaryCompare) + 1)
        sTitle = Trim$(Replace_MP3(sTmp))
    ElseIf sName Like "[*]*" Then
        sTmp = Mid$(sName, 2)
        sTmp = Left$(sTmp, InStr(1, sTmp, "]", vbBinaryCompare) - 1)
        sArtist = Trim$(Replace_MP3(sTmp))
        sTmp = Mid$(sName, InStr(1, sName, "[", vbBinaryCompare) + 1)
        sTitle = Trim$(Replace_MP3(sTmp))
    Else
        sArtist = ""
        sTitle = ""
        File_MakeTag = False
    End If
End Function

Public Function File_ReadTag(ByVal sFilePath As String, tFileTag As mp3Tag) As Long
    Dim lFileNumber&, lFileSize&
    
On Error GoTo ErrorIO
    lFileNumber = FreeFile
    Open sFilePath For Binary Access Read As lFileNumber
    lFileSize = LOF(lFileNumber)
    Get lFileNumber, lFileSize - Len(tFileTag) + 1, tFileTag
    Close lFileNumber
    
    Exit Function
ErrorIO:
    Reset
    File_ReadTag = Err.Number
End Function

Public Function File_SaveTag(ByVal sFilePath As String, tFileTag As mp3Tag, Optional bHadTag As Boolean = True) As Long
    Dim lFileNumber&, lFileSize&

On Error GoTo ErrorIO
    lFileNumber = FreeFile
    Open sFilePath For Binary Access Write As lFileNumber
    lFileSize = LOF(lFileNumber)
    
    If bHadTag Then
        Seek lFileNumber, lFileSize - Len(tFileTag) + 1
    Else
        Seek lFileNumber, lFileSize + 1
    End If
    
    Put lFileNumber, , tFileTag
    Close lFileNumber
    Exit Function
ErrorIO:
    Reset
    File_SaveTag = Err.Number
End Function

Public Sub File_Truncate(ByVal sPath As String)
    Dim lFileSize&, lHandle&
    Dim tSecu As SECURITY_ATTRIBUTES
    
    lFileSize = FileLen(sPath)
    tSecu.nLength = Len(tSecu)
    
    lHandle = CreateFile(sPath, GENERIC_WRITE, 0&, tSecu, OPEN_EXISTING, FILE_ATTRIBUTE_ARCHIVE, 0&)
    lFileSize = SetFilePointer(lHandle, lFileSize - 128, ByVal 0&, FILE_BEGIN)
    SetEndOfFile lHandle
    CloseHandle lHandle
End Sub

Public Function Replace_MP3(ByVal sString As String) As String
    Dim sReturn$
    Dim sChar As String * 1
    Dim sSpace As String * 3
    Dim I&
    
    'Replace "%20" by " " for mnemonics
    For I = 1 To Len(sString)
        sChar = Mid$(sString, I, 1)
        If sChar = "%" Then
            sSpace = Mid$(sString, I, 3)
            If sSpace = "%20" Then
                sReturn = sReturn & " "
                I = I + 2
            Else
                sReturn = sReturn & sChar
            End If
        Else
            sReturn = sReturn & sChar
        End If
    Next
    sString = sReturn
    sReturn = ""
    
    'Replace "_" by " "
    For I = 1 To Len(sString)
        sChar = Mid$(sString, I, 1)
        If sChar = "_" Then sReturn = sReturn & " " Else: sReturn = sReturn & sChar
    Next
    
    Replace_MP3 = sReturn
End Function

Public Function Replace_Title(ByVal sString As String) As String
    Dim sReturn$
    Dim sChar As String * 1
    Dim I&
    
    'Replace "&" by "&&" for mnemonics
    For I = 1 To Len(sString)
        sChar = Mid$(sString, I, 1)
        If sChar = "&" Then sReturn = sReturn & "&"
        sReturn = sReturn & sChar
    Next
    sString = sReturn
    sReturn = ""
    
    'Replace "_" by " "
    For I = 1 To Len(sString)
        sChar = Mid$(sString, I, 1)
        If sChar = "_" Then sReturn = sReturn & " " Else: sReturn = sReturn & sChar
    Next
    
    Replace_Title = sReturn
End Function
