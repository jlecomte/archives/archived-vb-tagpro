VERSION 5.00
Begin VB.Form frmPreferences 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Preferences"
   ClientHeight    =   2370
   ClientLeft      =   45
   ClientTop       =   285
   ClientWidth     =   4200
   ControlBox      =   0   'False
   Icon            =   "frmPreferences.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2370
   ScaleWidth      =   4200
   StartUpPosition =   1  'CenterOwner
   Begin VB.CommandButton cmdNext 
      Caption         =   "Next >>"
      Height          =   375
      Left            =   120
      TabIndex        =   11
      Top             =   1920
      Width           =   1095
   End
   Begin VB.Frame fraOptions 
      Height          =   1815
      Index           =   1
      Left            =   120
      TabIndex        =   7
      Top             =   0
      Visible         =   0   'False
      Width           =   3975
      Begin VB.CommandButton cmdContextMenu 
         Caption         =   "Set TagPro as context menu for .mp3 files"
         Height          =   255
         Left            =   120
         TabIndex        =   12
         Top             =   240
         Width           =   3615
      End
      Begin VB.CheckBox chkShowName 
         Caption         =   "Show name in caption bar."
         Height          =   255
         Left            =   120
         TabIndex        =   10
         Top             =   1320
         Width           =   2295
      End
      Begin VB.CheckBox chkDirectory 
         Caption         =   "Start with last directory."
         Height          =   255
         Left            =   120
         TabIndex        =   9
         Top             =   960
         Width           =   2055
      End
      Begin VB.CheckBox chkTray 
         Caption         =   "Reduce application to tray bar."
         Height          =   255
         Left            =   120
         TabIndex        =   8
         Top             =   600
         Width           =   2535
      End
   End
   Begin VB.CommandButton cmdOk 
      Caption         =   "&OK"
      Default         =   -1  'True
      Height          =   375
      Left            =   3000
      TabIndex        =   0
      Top             =   1920
      Width           =   1095
   End
   Begin VB.CommandButton cmdCancel 
      Caption         =   "&Cancel"
      Height          =   375
      Left            =   1800
      TabIndex        =   1
      Top             =   1920
      Width           =   1095
   End
   Begin VB.Frame fraOptions 
      Height          =   1815
      Index           =   0
      Left            =   120
      TabIndex        =   2
      Top             =   0
      Width           =   3975
      Begin VB.ComboBox cmbAuto 
         Height          =   315
         Index           =   0
         Left            =   120
         Style           =   2  'Dropdown List
         TabIndex        =   6
         Top             =   240
         Width           =   3615
      End
      Begin VB.ComboBox cmbAuto 
         Height          =   315
         Index           =   1
         Left            =   120
         Style           =   2  'Dropdown List
         TabIndex        =   5
         Top             =   600
         Width           =   3615
      End
      Begin VB.ComboBox cmbAuto 
         Height          =   315
         Index           =   2
         Left            =   120
         Style           =   2  'Dropdown List
         TabIndex        =   4
         Top             =   960
         Width           =   3615
      End
      Begin VB.ComboBox cmbAuto 
         Height          =   315
         Index           =   3
         Left            =   120
         Style           =   2  'Dropdown List
         TabIndex        =   3
         Top             =   1320
         Width           =   3615
      End
   End
End
Attribute VB_Name = "frmPreferences"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
'  Copyright (C) 2000-2004, Julien Lecomte
'
'  This software Is provided 'as-is', without any express or implied
'  warranty.  In no event will the authors be held liable for any damages
'  arising from the use of this software.
'
'  Permission is granted to anyone to use this software for any purpose,
'  including commercial applications, and to alter it and redistribute it
'  freely, subject to the following restrictions:
'
'  1. The origin of this software must not be misrepresented; you must not
'     claim that you wrote the original software. If you use this software
'     in a product, an acknowledgment in the product documentation would be
'     appreciated but is not required.
'  2. Altered source versions must be plainly marked as such, and must not be
'     misrepresented as being the original software.
'  3. This notice may not be removed or altered from any source distribution.
'
Private Sub cmdCancel_Click()
    Unload Me
End Sub

Private Sub cmdContextMenu_Click()
    Dim sDefault$
    Dim sPathIcon$
    Dim sPathOpen$
    Const sRegRoot = "Winamp.file"
    Const sRegType = "MP3 Music file"
    Const sEdit = "Edit in TagPro"
    
    sPathOpen = AddSlash(App.Path) & App.EXEName & " %1"
    GetRegistry HKEY_CLASSES_ROOT, ".mp3", "", sDefault
    If sDefault = "" Then
        sDefault = sRegRoot
        'Don't do icon.
'        sPathIcon = AddSlash(App.Path) & App.EXEName & ",1"
        SetRegistry HKEY_CLASSES_ROOT, ".mp3", "", sRegRoot, REG_SZ
        SetRegistry HKEY_CLASSES_ROOT, sDefault, "", sRegType, REG_SZ
        SetRegistry HKEY_CLASSES_ROOT, sDefault & "\DefaultIcon", "", sPathIcon, REG_SZ
        SetRegistry HKEY_CLASSES_ROOT, sDefault & "\shell\Edit", "", sEdit, REG_SZ
        SetRegistry HKEY_CLASSES_ROOT, sDefault & "\shell\Edit\command", "", sPathOpen, REG_SZ
    Else
        SetRegistry HKEY_CLASSES_ROOT, sDefault & "\shell\Edit", "", sEdit, REG_SZ
        SetRegistry HKEY_CLASSES_ROOT, sDefault & "\shell\Edit\command", "", sPathOpen, REG_SZ
    End If
End Sub

Private Sub cmdNext_Click()
    fraOptions(0).Visible = Not fraOptions(0).Visible
    fraOptions(1).Visible = Not fraOptions(1).Visible
    Select Case fraOptions(0).Visible
        Case True
            cmdNext.Caption = "Next >>"
        Case False
            cmdNext.Caption = "<< Next"
    End Select
End Sub

Private Sub cmdOk_Click()
    Dim I&
    Dim sOpt(0 To 4) As String
    sOpt(0) = "AutoSave"
    sOpt(1) = "AutoMake"
    sOpt(2) = "AutoApply"
    sOpt(3) = "BrowseWindow"
    For I = 0 To 3
        SetOption sOpt(I), True, False, cmbAuto(I).ListIndex 'Value of true or false is not important
    Next
    SetRegistry HKEY_CURRENT_USER, sRegistryWindow, "Reduce to Tray", chkTray.Value, REG_DWORD
    SetRegistry HKEY_CURRENT_USER, sRegistryOptions, "Open last", chkDirectory.Value, REG_DWORD
    SetRegistry HKEY_CURRENT_USER, sRegistryOptions, "See Caption", chkShowName.Value, REG_DWORD
    bOptionSeeCaption = CBool(chkShowName.Value)
    
    Unload Me
End Sub

Private Sub Form_Load()
    Dim I&
    Dim lVal&
    Dim byteReturn As Byte
    Dim sOpt(0 To 4) As String
    Dim sKey(0 To 4) As String
    sOpt(0) = "AutoSave": sKey(0) = sOpt(0)
    sOpt(1) = "AutoMake": sKey(1) = sOpt(1)
    sOpt(2) = "AutoApply": sKey(2) = sOpt(2)
    sOpt(3) = "BrowseWindow": sKey(3) = "Browse Window"
    For I = 0 To 2
        cmbAuto(I).AddItem sKey(I) & " selected depends of last use."
        cmbAuto(I).AddItem sKey(I) & " selected upon start up."
        cmbAuto(I).AddItem sKey(I) & " unselected upon start up."
        GetOption sOpt(I), byteReturn
        cmbAuto(I).ListIndex = byteReturn
    Next
    For I = 3 To 3
        cmbAuto(I).AddItem sKey(I) & " visible depends of last use."
        cmbAuto(I).AddItem sKey(I) & " visible upon start up."
        cmbAuto(I).AddItem sKey(I) & " hidden upon start up."
        GetOption sOpt(I), byteReturn
        cmbAuto(I).ListIndex = byteReturn
    Next
    GetRegistry HKEY_CURRENT_USER, sRegistryWindow, "Reduce to Tray", lVal, vbChecked
    chkTray.Value = lVal
    GetRegistry HKEY_CURRENT_USER, sRegistryOptions, "Open last", lVal, vbChecked
    chkDirectory.Value = lVal
    chkShowName.Value = Abs(CLng(bOptionSeeCaption))
End Sub
