VERSION 5.00
Begin VB.Form frmMain 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "TagPro"
   ClientHeight    =   4125
   ClientLeft      =   150
   ClientTop       =   765
   ClientWidth     =   9150
   Icon            =   "frmMain.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   ScaleHeight     =   275
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   610
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton cmdLock 
      Height          =   255
      Left            =   4920
      Picture         =   "frmMain.frx":0A02
      Style           =   1  'Graphical
      TabIndex        =   27
      TabStop         =   0   'False
      Top             =   3840
      UseMaskColor    =   -1  'True
      Width           =   255
   End
   Begin VB.FileListBox lbFiles 
      Height          =   3795
      Left            =   2520
      Pattern         =   "*.mp3"
      TabIndex        =   2
      TabStop         =   0   'False
      Top             =   0
      Width           =   2655
   End
   Begin VB.DirListBox lbDirectory 
      Height          =   3690
      Left            =   120
      TabIndex        =   1
      TabStop         =   0   'False
      Top             =   360
      Width           =   2295
   End
   Begin VB.DriveListBox lbDrive 
      Height          =   315
      Left            =   120
      TabIndex        =   0
      TabStop         =   0   'False
      Top             =   0
      Width           =   2295
   End
   Begin VB.Frame fraTagOptions 
      Height          =   4095
      Left            =   5280
      TabIndex        =   19
      Top             =   0
      Width           =   3855
      Begin VB.Frame fraApply 
         Caption         =   "Apply"
         Height          =   735
         Left            =   120
         TabIndex        =   26
         Top             =   3240
         Width           =   3615
         Begin VB.CheckBox chkTrack 
            Caption         =   "#"
            Height          =   195
            Left            =   2760
            TabIndex        =   34
            Top             =   480
            Width           =   495
         End
         Begin VB.CheckBox chkComment 
            Caption         =   "Comment"
            Height          =   195
            Left            =   1800
            TabIndex        =   18
            Top             =   480
            Value           =   1  'Checked
            Width           =   975
         End
         Begin VB.CheckBox chkGenre 
            Caption         =   "Genre"
            Height          =   195
            Left            =   1800
            TabIndex        =   15
            Top             =   240
            Value           =   1  'Checked
            Width           =   975
         End
         Begin VB.CheckBox chkYear 
            Caption         =   "Year"
            Height          =   195
            Left            =   960
            TabIndex        =   17
            Top             =   480
            Value           =   1  'Checked
            Width           =   855
         End
         Begin VB.CheckBox chkAlbum 
            Caption         =   "Album"
            Height          =   195
            Left            =   960
            TabIndex        =   14
            Top             =   240
            Value           =   1  'Checked
            Width           =   855
         End
         Begin VB.CheckBox chkArtist 
            Caption         =   "Artist"
            Height          =   195
            Left            =   120
            TabIndex        =   16
            Top             =   480
            Value           =   1  'Checked
            Width           =   735
         End
         Begin VB.CheckBox chkTitle 
            Caption         =   "Title"
            Height          =   195
            Left            =   120
            TabIndex        =   13
            Top             =   240
            Width           =   735
         End
      End
      Begin VB.CommandButton cmdMemorize 
         Caption         =   "M&emorize"
         Height          =   375
         Left            =   2520
         TabIndex        =   12
         ToolTipText     =   "Memorize this ID tag"
         Top             =   2760
         Visible         =   0   'False
         Width           =   1095
      End
      Begin VB.CommandButton cmdApply 
         Caption         =   "&Apply"
         Enabled         =   0   'False
         Height          =   375
         Left            =   1320
         TabIndex        =   11
         ToolTipText     =   "Apply the memorized ID tag"
         Top             =   2760
         Width           =   1095
      End
      Begin VB.CommandButton cmdRemove 
         Caption         =   "&Remove"
         Enabled         =   0   'False
         Height          =   375
         Left            =   120
         TabIndex        =   10
         ToolTipText     =   "Remove the ID tag"
         Top             =   2760
         Width           =   1095
      End
      Begin VB.CommandButton cmdMake 
         Caption         =   "&Make"
         Enabled         =   0   'False
         Height          =   375
         Left            =   2520
         TabIndex        =   9
         ToolTipText     =   "Create the ID from file name"
         Top             =   2280
         Width           =   1095
      End
      Begin VB.CommandButton cmdCancel 
         Caption         =   "&Cancel"
         Enabled         =   0   'False
         Height          =   375
         Left            =   1320
         TabIndex        =   8
         ToolTipText     =   "Restore old ID tag"
         Top             =   2280
         Width           =   1095
      End
      Begin VB.CommandButton cmdSave 
         Caption         =   "&Save"
         Enabled         =   0   'False
         Height          =   375
         Left            =   120
         TabIndex        =   7
         ToolTipText     =   "Save current ID tag"
         Top             =   2280
         Width           =   1095
      End
      Begin VB.Frame fraTag 
         BorderStyle     =   0  'None
         Height          =   1815
         Left            =   120
         TabIndex        =   21
         Top             =   240
         Visible         =   0   'False
         Width           =   3615
         Begin VB.TextBox txtAlbum 
            Height          =   285
            Left            =   765
            MaxLength       =   30
            TabIndex        =   32
            Top             =   720
            Width           =   2655
         End
         Begin VB.TextBox txtTitle 
            Height          =   285
            Left            =   765
            MaxLength       =   30
            TabIndex        =   30
            Top             =   0
            Width           =   2655
         End
         Begin VB.TextBox txtTrack 
            Height          =   285
            Left            =   3120
            MaxLength       =   2
            TabIndex        =   29
            Top             =   1440
            Width           =   300
         End
         Begin VB.ComboBox cmbGenre 
            Height          =   315
            Left            =   1800
            Sorted          =   -1  'True
            Style           =   2  'Dropdown List
            TabIndex        =   5
            Top             =   1080
            Width           =   1575
         End
         Begin VB.TextBox txtComment 
            Height          =   285
            Left            =   765
            MaxLength       =   28
            TabIndex        =   6
            Top             =   1440
            Width           =   2100
         End
         Begin VB.TextBox txtArtist 
            Height          =   285
            Left            =   765
            MaxLength       =   30
            TabIndex        =   3
            Top             =   360
            Width           =   2655
         End
         Begin VB.TextBox txtYear 
            Height          =   285
            Left            =   765
            MaxLength       =   4
            TabIndex        =   4
            Top             =   1080
            Width           =   495
         End
         Begin VB.Label lblAlbum 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "Album"
            Height          =   195
            Left            =   240
            TabIndex        =   33
            Top             =   765
            UseMnemonic     =   0   'False
            Width           =   435
         End
         Begin VB.Label lblTitle 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "Title"
            Height          =   195
            Left            =   360
            TabIndex        =   31
            Top             =   45
            UseMnemonic     =   0   'False
            Width           =   300
         End
         Begin VB.Label lblTrack 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "#"
            Height          =   195
            Left            =   2880
            TabIndex        =   28
            Top             =   1485
            UseMnemonic     =   0   'False
            Width           =   225
         End
         Begin VB.Label lblGenre 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "Genre"
            Height          =   195
            Left            =   1320
            TabIndex        =   25
            Top             =   1140
            UseMnemonic     =   0   'False
            Width           =   435
         End
         Begin VB.Label lblComment 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "Comment"
            Height          =   195
            Left            =   0
            TabIndex        =   24
            Top             =   1485
            UseMnemonic     =   0   'False
            Width           =   660
         End
         Begin VB.Label lblYear 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "Year"
            Height          =   195
            Left            =   330
            TabIndex        =   23
            Top             =   1125
            UseMnemonic     =   0   'False
            Width           =   330
         End
         Begin VB.Label lblArtist 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "Artist"
            Height          =   195
            Left            =   315
            TabIndex        =   22
            Top             =   405
            UseMnemonic     =   0   'False
            Width           =   345
         End
      End
   End
   Begin VB.Label lblFound 
      Caption         =   "Number of files found: 0"
      Height          =   255
      Left            =   2520
      TabIndex        =   20
      Top             =   3840
      Width           =   2295
   End
   Begin VB.Menu mnuFile 
      Caption         =   "&File"
      Begin VB.Menu mnuFileNameFiles 
         Caption         =   "&Name files..."
         Visible         =   0   'False
      End
      Begin VB.Menu mnuFileBatchDirectory 
         Caption         =   "&Batch directory..."
      End
      Begin VB.Menu mnuFileMakeIndex 
         Caption         =   "&Make index..."
      End
      Begin VB.Menu mnuTiret2 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFileQuit 
         Caption         =   "&Quit"
         Shortcut        =   ^Q
      End
   End
   Begin VB.Menu mnuOptions 
      Caption         =   "&Options"
      Begin VB.Menu mnuOptionsAutoSave 
         Caption         =   "Auto &save"
      End
      Begin VB.Menu mnuOptionsAutoMake 
         Caption         =   "Auto &make"
      End
      Begin VB.Menu mnuOptionsAutoApply 
         Caption         =   "Auto &apply"
      End
      Begin VB.Menu mnuTiret0 
         Caption         =   "-"
      End
      Begin VB.Menu mnuOptionsPreferences 
         Caption         =   "&Preferences..."
      End
   End
   Begin VB.Menu mnuView 
      Caption         =   "&View"
      Begin VB.Menu mnuViewBrowseWindow 
         Caption         =   "&Browse window"
         Checked         =   -1  'True
      End
      Begin VB.Menu mnuViewMemory 
         Caption         =   "&Memory tag..."
      End
   End
   Begin VB.Menu mnuHelp 
      Caption         =   "&?"
      Begin VB.Menu mnuHelpAbout 
         Caption         =   "About..."
         Shortcut        =   {F1}
      End
      Begin VB.Menu mnuHelpOnlineHelp 
         Caption         =   "Online help"
      End
   End
   Begin VB.Menu mnuSystemTray 
      Caption         =   "TagPro"
      Visible         =   0   'False
      Begin VB.Menu mnuSystemTrayAbout 
         Caption         =   "&About"
      End
      Begin VB.Menu mnuSystemTrayRestore 
         Caption         =   "&Restore"
      End
      Begin VB.Menu mnuSystemQuit 
         Caption         =   "&Quit"
      End
   End
End
Attribute VB_Name = "frmMain"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
'  Copyright (C) 2000-2004, Julien Lecomte
'
'  This software Is provided 'as-is', without any express or implied
'  warranty.  In no event will the authors be held liable for any damages
'  arising from the use of this software.
'
'  Permission is granted to anyone to use this software for any purpose,
'  including commercial applications, and to alter it and redistribute it
'  freely, subject to the following restrictions:
'
'  1. The origin of this software must not be misrepresented; you must not
'     claim that you wrote the original software. If you use this software
'     in a product, an acknowledgment in the product documentation would be
'     appreciated but is not required.
'  2. Altered source versions must be plainly marked as such, and must not be
'     misrepresented as being the original software.
'  3. This notice may not be removed or altered from any source distribution.
'
Private tOpenTag As mp3Tag
Private bHasTag  As Boolean
Private bDirty   As Boolean
Private bOriginalTag As Boolean
Private sOldPath As String
Private VBGTray  As NOTIFYICONDATA
Private bEnabled As Boolean

Private Sub ClearFrame()
    Caption = "TagPro"
    sOldPath = ""
    fraTagOptions.Caption = ""
    fraTagOptions.ToolTipText = ""
    txtTitle = ""
    txtArtist = ""
    txtAlbum = ""
    txtComment = ""
    txtTitle = ""
    txtYear = ""
    txtTrack = ""
    cmbGenre.ListIndex = -1
    cmdSave.Enabled = False
    cmdApply.Enabled = False
    cmdCancel.Enabled = False
    cmdMake.Enabled = False
    cmdRemove.Enabled = False
    bDirty = False
    fraTag.Visible = False
    cmdMemorize.Visible = False
    cmdLock.Visible = False
End Sub

Private Sub cmbGenre_Change()
    bDirty = True
End Sub

Private Sub cmbGenre_Click()
    bDirty = True
End Sub

'REFRESH APPY BUTTON IN CASE OF MAKE CLICK

Private Sub cmdApply_Click()
    If chkGenre Then cmbGenre.ListIndex = tMemTag.bGenre
    If chkAlbum Then txtAlbum = Trim$(tMemTag.sAlbum)
    If chkArtist Then txtArtist = Trim$(tMemTag.sArtist)
    If chkComment Then txtComment = Trim$(tMemTag.sComment)
    If chkTitle Then txtTitle = Trim$(tMemTag.sTitle)
    If chkYear Then txtYear = Trim$(tMemTag.sYear)
    bHasTag = True
    fraTag.Visible = True
    cmdMemorize.Visible = True
End Sub

Private Sub cmdCancel_Click()
    Select Case bOriginalTag
        Case True
            txtAlbum = Trim$(tOpenTag.sAlbum)
            txtArtist = Trim$(tOpenTag.sArtist)
            txtComment = Trim$(tOpenTag.sComment)
            txtTitle = Trim$(tOpenTag.sTitle)
            txtYear = Trim$(tOpenTag.sYear)
            GenreItem tOpenTag.bGenre
            bHasTag = True
            fraTag.Visible = True
            cmdMemorize.Visible = True
        Case False
            txtAlbum = ""
            txtArtist = ""
            txtComment = ""
            txtTitle = ""
            txtYear = ""
            cmbGenre.ListIndex = 0
            bHasTag = False
            fraTag.Visible = False
            cmdMemorize.Visible = False
    End Select
    bDirty = False
End Sub

Private Sub cmdLock_Click()
    Dim sName$, sPath$
    Dim lAttr&
On Error GoTo Error
    sName = lbFiles.List(lbFiles.ListIndex)
    sPath = AddSlash(lbDirectory) & sName
    lAttr = GetAttr(sPath)
    If lAttr And vbReadOnly Then
        lAttr = lAttr And Not vbReadOnly
    Else
        lAttr = lAttr Or vbReadOnly
    End If
    SetAttr sPath, lAttr
    
    bEnabled = Not bEnabled
    EnableButtons bEnabled
    lbFiles.SetFocus
    Exit Sub
Error:
    MsgBox "Can't set attributes.", vbOKOnly
    cmdLock.Visible = False
    lbFiles.SetFocus
End Sub

Private Sub cmdMake_Click()
    Dim sName$, sTmp$
    Dim sTitle$, sArtist$
    Dim bReturn As Boolean
    
    sName = lbFiles.List(lbFiles.ListIndex)
    sName = Left$(sName, Len(sName) - 4)
    
    bReturn = File_MakeTag(sName, sTitle, sArtist)
    
    txtArtist = IIf(bReturn, sArtist, "")
    txtTitle = IIf(bReturn, sTitle, "")
    txtAlbum = ""
    txtComment = ""
    txtYear = ""
    cmbGenre.ListIndex = 0
    
    bHasTag = True
    fraTag.Visible = True
    cmdMemorize.Visible = True
    If sOldPath <> "" And cmdSave.Enabled Then cmdApply.Enabled = True
End Sub

Private Sub cmdMemorize_Click()
    tMemTag.sTag = "TAG"
    tMemTag.bGenre = cmbGenre.ListIndex
    tMemTag.sAlbum = txtAlbum
    tMemTag.sArtist = txtArtist
    tMemTag.sComment = txtComment
    tMemTag.sTitle = txtTitle
    tMemTag.sYear = txtYear
    MemTagToolTip
    If sOldPath <> "" And cmdSave.Enabled Then cmdApply.Enabled = True
End Sub

Private Sub cmdRemove_Click()
    txtAlbum = ""
    txtArtist = ""
    txtComment = ""
    txtTitle = ""
    txtYear = ""
    cmbGenre.ListIndex = -1
    bHasTag = False
    fraTag.Visible = False
    cmdMemorize.Visible = False
End Sub

Private Sub cmdSave_Click()
    Dim sName$, sPath$
    sName = lbFiles.List(lbFiles.ListIndex)
    sPath = lbDirectory & IIf(Right$(lbDirectory, 1) = "\", "", "\") & sName
    SaveFile sPath
End Sub

Private Sub EnableButtons(bValue As Boolean)
    bEnabled = bValue
    cmdLock.Visible = True
    cmdLock.Picture = LoadResPicture(IIf(bValue, "LOCK", "UNLOCK"), vbResBitmap)
    cmdSave.Enabled = bValue
    cmdCancel.Enabled = bValue
    cmdMake.Enabled = bValue
    cmdRemove.Enabled = bValue
    If tMemTag.sTag <> "TAG" Then cmdApply.Enabled = False Else cmdApply.Enabled = bValue
End Sub

Private Sub Form_Load()
    Dim lStyle&, I&
    Dim lRetVal&
    Dim lTop&, lLeft&
    Dim sLastDirectory$
    
#If debugon = False Then
    Dim lhSysMenu&
    lhSysMenu = GetSystemMenu(hwnd, 0&)
    AppendMenu lhSysMenu, MF_SEPARATOR, 0&, vbNullString
    AppendMenu lhSysMenu, MF_STRING, IDM_ABOUT, "About " & App.Title
    AppendMenu lhSysMenu, MF_STRING, IDM_TRAY, "Tray"
    lProcOld = SetWindowLong(hwnd, GWL_WNDPROC, AddressOf SysMenuHandler)
    DeleteMenu lhSysMenu, 5, MF_BYPOSITION
    DeleteMenu lhSysMenu, 4, MF_BYPOSITION
    DeleteMenu lhSysMenu, 2, MF_BYPOSITION
    DeleteMenu lhSysMenu, 0, MF_BYPOSITION
#End If
    
    'Initialize locations in frame
    lblTitle.Top = txtTitle.Top + (txtTitle.Height - lblTitle.Height) \ 2
    lblAlbum.Top = txtAlbum.Top + (txtAlbum.Height - lblAlbum.Height) \ 2
    lblArtist.Top = txtArtist.Top + (txtArtist.Height - lblArtist.Height) \ 2
    lblComment.Top = txtComment.Top + (txtComment.Height - lblComment.Height) \ 2
    lblYear.Top = txtYear.Top + (txtYear.Height - lblYear.Height) \ 2
    lblGenre.Top = cmbGenre.Top + (cmbGenre.Height - lblGenre.Height) \ 2
    cmbGenre.Left = txtComment.Left + txtComment.Width - cmbGenre.Width
    lStyle = GetWindowLong(txtYear.hwnd, GWL_STYLE)
    SetWindowLong txtYear.hwnd, GWL_STYLE, lStyle Or ES_NUMBER
    
    cmbGenre.AddItem ""
    cmbGenre.ItemData(cmbGenre.NewIndex) = 255
    For I = 1 To saGenreLimit
        cmbGenre.AddItem saGenres(I)
        cmbGenre.ItemData(cmbGenre.NewIndex) = I
    Next
    cmbGenre.ListIndex = 0
    VBGTray.cbSize = Len(VBGTray)
    VBGTray.hwnd = hwnd
    VBGTray.uId = vbNull
    
    With tMemTag
        .sAlbum = ""
        .sArtist = ""
        .sComment = ""
        .sTag = ""
        .sTitle = ""
        .sYear = ""
        .bGenre = 0
    End With
    
    'Initialize locations in form
    lbDrive.Move 5
    lbDirectory.Move 5
    
    GetRegistry HKEY_CURRENT_USER, sRegistryOptions, "Open last", lRetVal, 0
    If lRetVal Then
        lRetVal = GetRegistry(HKEY_CURRENT_USER, sRegistryTagPro, "Last Directory", sLastDirectory)
        If lRetVal = REG_SZ Then
            If Dir$(sLastDirectory, vbDirectory) <> "" Then
                lbDrive.Drive = Left$(sLastDirectory, 3)
                lbDirectory.Path = sLastDirectory
            End If
        End If
    End If
    
    mnuOptionsAutoSave.Checked = GetOption("AutoSave", CByte(0))
    mnuOptionsAutoApply.Checked = GetOption("AutoApply", CByte(0))
    mnuOptionsAutoMake.Checked = GetOption("AutoMake", CByte(0))
    GetRegistry HKEY_CURRENT_USER, sRegistryOptions, "See Caption", lRetVal, 1
    bOptionSeeCaption = CBool(lRetVal)
    
    If Not GetOption("BrowseWindow", CByte(0), True) Then mnuViewBrowseWindow_Click  'Inverted
    
    lRetVal = GetRegistry(HKEY_CURRENT_USER, sRegistryWindow, "Top", lTop)
    If lRetVal = REG_DWORD Then
        lRetVal = GetRegistry(HKEY_CURRENT_USER, sRegistryWindow, "Top", lLeft)
        If lRetVal = REG_DWORD Then
            If lLeft < Screen.Width And lTop < Screen.Height Then Move lLeft, lTop
        End If
    End If
End Sub

Private Sub Form_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    Static blnFlag As Boolean
    
    If Not blnFlag Then
        blnFlag = True
        Select Case X
            Case WM_LBUTTONDBLCLICK
                Tray False
            Case WM_RBUTTONUP
                SetForegroundWindow hwnd
                PopupMenu mnuSystemTray
        End Select
        blnFlag = False
    End If
End Sub

Private Sub Form_Resize()
    Dim lVal&
    
    If frmMain.WindowState = vbMinimized Then
        GetRegistry HKEY_CURRENT_USER, sRegistryWindow, "Reduce to Tray", lVal, vbChecked
        If lVal = vbChecked Then Tray True
    End If
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Dim sLastDirectory$
    
#If debugon = False Then
    SetWindowLong hwnd, GWL_WNDPROC, lProcOld
#End If

    If mnuOptionsAutoSave.Checked And sOldPath <> "" Then SaveFile sOldPath
    
    sLastDirectory = lbDirectory.Path
    SetRegistry HKEY_CURRENT_USER, sRegistryTagPro, "Last Directory", sLastDirectory, REG_SZ
    SetRegistry HKEY_CURRENT_USER, sRegistryWindow, "Top", CLng(frmMain.Top), REG_DWORD
    SetRegistry HKEY_CURRENT_USER, sRegistryWindow, "Left", CLng(frmMain.Left), REG_DWORD

    SetOption "AutoSave", mnuOptionsAutoSave.Checked
    SetOption "AutoApply", mnuOptionsAutoApply.Checked
    SetOption "AutoMake", mnuOptionsAutoMake.Checked
    SetOption "BrowseWindow", mnuViewBrowseWindow.Checked  'not inverted

    Set frmAbout = Nothing
    Set frmBatchDirectory = Nothing
    Set frmMakeIndex = Nothing
    Set frmMemTag = Nothing
    Set frmPreferences = Nothing
    
    Set frmMain = Nothing
End Sub

Private Sub GenreItem(ByVal bGenre As Byte)
    Dim I&
    
    If bGenre > saGenreLimit Then
        cmbGenre.ListIndex = 0
        Exit Sub
    Else
        For I = 1 To saGenreLimit
            If cmbGenre.ItemData(I) = bGenre Then
                cmbGenre.ListIndex = I
                Exit Sub
            End If
        Next
    End If
End Sub

Private Sub lbDirectory_Change()
    If mnuOptionsAutoSave.Checked And sOldPath <> "" Then SaveFile sOldPath
    ClearFrame
On Error GoTo ErrDirectory
    lbFiles.Path = lbDirectory.Path
    lblFound = "Number of files found: " & lbFiles.ListCount
Exit Sub
ErrDirectory:
    ErrHandler Err
    lbDirectory.Refresh
End Sub

Private Sub lbDirectory_Click()
On Error GoTo ErrDirectory
    lbDirectory.Path = lbDirectory.List(lbDirectory.ListIndex)
Exit Sub
ErrDirectory:
    ErrHandler Err
    lbDirectory.Refresh
End Sub

Private Sub lbDrive_Change()
On Error GoTo ErrDrive
    lbDirectory.Path = Left$(lbDrive.Drive, 2) & "\"
Exit Sub
ErrDrive:
    ErrHandler Err
    lbDrive.Refresh
    lbDirectory.Refresh
End Sub

Private Sub lbFiles_Click()
    Dim sName$, sPath$
    Dim lIndex&
    Dim lRetVal&
    Dim tTag As mp3Tag
    Static bDblClick As Boolean
    
    If bDblClick Then Exit Sub
    bDblClick = True
    MousePointer = vbHourglass
On Error GoTo ErrFile
    If lbFiles.ListIndex = -1 Then
        lbFiles.Refresh
        lblFound = "Number of files found: " & lbFiles.ListCount
        ClearFrame
        MousePointer = vbNormal
        bDblClick = False
        Exit Sub
    End If
    lIndex = lbFiles.ListIndex
    sName = lbFiles.List(lIndex)
    sName = Left$(sName, Len(sName) - 4)
    fraTagOptions.Caption = Replace_Title(sName)
    fraTagOptions.ToolTipText = sName
    If bOptionSeeCaption Then Caption = "TagPro [" & Replace_Title(sName) & "]"    'Keep in case of

    'Get the tag
    sPath = AddSlash(lbDirectory) & sName & ".mp3"
    If sOldPath = sPath Then
        MousePointer = vbNormal
        bDblClick = False
        Exit Sub
    End If
    
    'Save old if autosave
    If (sOldPath <> "") And mnuOptionsAutoSave.Checked Then SaveFile sOldPath
    
    If Dir$(sPath) = "" Then
        MsgBox "File """ & Get_FileTitle(sOldPath) & """ no longer exists. It may have been deleted or moved.", vbOKOnly + vbCritical, "Error."
        lbFiles.ListIndex = -1
        MousePointer = vbNormal
        bDblClick = False
        'Should refresh the whole list!
        Exit Sub
    End If
    
    
    sOldPath = sPath
    lRetVal = File_ReadTag(sPath, tTag)
    If lRetVal Then Err.Raise lRetVal

    tOpenTag = tTag
    If tTag.sTag = "TAG" Then
        bOriginalTag = True
    Else
        bOriginalTag = False
        With tTag
        .sAlbum = ""
        .sArtist = ""
        .sComment = ""
        .sTag = ""
        .sTitle = ""
        .sYear = ""
        .bGenre = 255
        End With
    End If

    If GetAttr(sPath) And vbReadOnly Then EnableButtons False Else EnableButtons True
    
    If mnuOptionsAutoMake.Checked And cmdMake.Enabled Then
        cmdMake_Click
        txtComment = Trim$(tTag.sComment)
        txtAlbum = Trim$(tTag.sAlbum)
        txtYear = Trim$(tTag.sYear)
        GenreItem tTag.bGenre
    Else
        'Analyze tag
        If tTag.sTag = "TAG" Then
            txtAlbum = Trim$(tTag.sAlbum)
            txtArtist = Trim$(tTag.sArtist)
            txtComment = Trim$(tTag.sComment)
            txtTitle = Trim$(tTag.sTitle)
            txtYear = Trim$(tTag.sYear)
            GenreItem tTag.bGenre
            bHasTag = True
            fraTag.Visible = True
            cmdMemorize.Visible = True
        Else
            txtAlbum = ""
            txtArtist = ""
            txtComment = ""
            txtTitle = ""
            txtYear = ""
            cmbGenre.ListIndex = -1
            fraTag.Visible = False
            bHasTag = False
            cmdMemorize.Visible = False
        End If
    End If
    If mnuOptionsAutoApply.Checked And cmdApply.Enabled Then cmdApply_Click
    
    bDirty = False
    MousePointer = vbNormal
    bDblClick = False
    Exit Sub
ErrFile:
    bDblClick = False
    MousePointer = vbNormal
    Reset
    ErrHandler Err
End Sub

Private Sub lbFiles_KeyUp(KeyCode As Integer, Shift As Integer)
    bDirty = True 'So autosave if selected
    lbFiles_Click
    MousePointer = vbNormal
End Sub

Private Sub lbFiles_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    Dim lXPt&, lYPt&
    Dim lIndex&
    
    If Button <> 0 Then Exit Sub
    'Form scalemode is Pixels...
    lXPt = X / Screen.TwipsPerPixelX
    lYPt = Y / Screen.TwipsPerPixelY

    With lbFiles
        lIndex = SendMessage(.hwnd, LB_ITEMFROMPOINT, 0, ByVal ((lYPt * 65536) + lXPt))
        If (lIndex >= 0) And (lIndex <= .ListCount) Then
            .ToolTipText = .List(lIndex)
        Else
            .ToolTipText = ""
        End If
    End With
End Sub

Private Sub MemTagToolTip()
    If tMemTag.sTag = "TAG" Then
        If Trim$(tMemTag.sArtist) <> "" Then chkArtist.ToolTipText = Trim$(tMemTag.sArtist) Else chkArtist.ToolTipText = ""
        If Trim$(tMemTag.sTitle) <> "" Then chkTitle.ToolTipText = Trim$(tMemTag.sTitle) Else chkTitle.ToolTipText = ""
        If Trim$(tMemTag.sAlbum) <> "" Then chkAlbum.ToolTipText = Trim$(tMemTag.sAlbum) Else chkAlbum.ToolTipText = ""
        If Trim$(tMemTag.sYear) <> "" Then chkYear.ToolTipText = Trim$(tMemTag.sYear) Else chkYear.ToolTipText = ""
        If Trim$(tMemTag.sComment) <> "" Then chkComment.ToolTipText = Trim$(tMemTag.sComment) Else chkComment.ToolTipText = ""
        If Trim$(tMemTag.sTrack) <> "" Then chkTrack.ToolTipText = Trim$(tMemTag.sTrack) Else chkTrack.ToolTipText = ""
        If tMemTag.bGenre <> 0 Then chkGenre.ToolTipText = saGenres(tMemTag.bGenre) Else chkGenre.ToolTipText = ""
    Else
        chkGenre.ToolTipText = ""
        chkAlbum.ToolTipText = ""
        chkArtist.ToolTipText = ""
        chkComment.ToolTipText = ""
        chkTrack.ToolTipText = ""
        chkTitle.ToolTipText = ""
        chkYear.ToolTipText = ""
    End If
End Sub

Private Sub mnuFileBatchDirectory_Click()
    Load frmBatchDirectory
    frmBatchDirectory.txtLocation = lbFiles.Path
    frmBatchDirectory.Show vbModal
    MemTagToolTip
End Sub

Private Sub mnuFileMakeIndex_Click()
    Load frmMakeIndex
    frmMakeIndex.txtLocation = lbFiles.Path
    frmMakeIndex.Show vbModal, frmMain
End Sub

Private Sub mnuFileNameFiles_Click()
    MsgBox "Not yet implemented"
End Sub

Private Sub mnuFileQuit_Click()
    Unload Me
End Sub

Private Sub mnuHelpAbout_Click()
    frmAbout.Show vbModal
End Sub

Private Sub mnuHelpOnlineHelp_Click()
    ShellExecute HWND_TOP, vbNullString, "http://free.prohosting.com/~jagsite/tagpro/help.htm", vbNullString, vbNullString, vbNormalFocus
End Sub

Private Sub mnuOptionsAutoApply_Click()
    mnuOptionsAutoApply.Checked = Not mnuOptionsAutoApply.Checked
End Sub

Private Sub mnuOptionsAutoMake_Click()
    mnuOptionsAutoMake.Checked = Not mnuOptionsAutoMake.Checked
End Sub

Private Sub mnuOptionsAutoSave_click()
    Dim lAns&
    
    If bDirty Then
        lAns = MsgBox("Do you wish to save tag before removing Autosave?", vbYesNo, "Save tag?")
        If lAns = vbYes Then
            If (sOldPath <> "") And mnuOptionsAutoSave.Checked Then SaveFile sOldPath
        End If
    End If
    mnuOptionsAutoSave.Checked = Not mnuOptionsAutoSave.Checked
End Sub

Private Sub mnuOptionsPreferences_Click()
    frmPreferences.Show vbModal
    If Not bOptionSeeCaption Then Caption = "TagPro"
End Sub

Private Sub mnuSystemQuit_Click()
    Tray False, False
    Unload Me
End Sub

Private Sub mnuSystemTrayAbout_Click()
    frmAbout.Show vbModal
End Sub

Private Sub mnuSystemTrayRestore_Click()
    Tray False
End Sub

Private Sub mnuViewBrowseWindow_Click()
    mnuViewBrowseWindow.Checked = Not mnuViewBrowseWindow.Checked
    Select Case mnuViewBrowseWindow.Checked
        Case False
            lbDrive.Visible = False
            lbDirectory.Visible = False
            lbFiles.Move lbDrive.Left
            lblFound.Move lbDrive.Left
        Case True
            lbDrive.Visible = True
            lbDirectory.Visible = True
            lbFiles.Move lbDrive.Left + lbDrive.Width + 5
            lblFound.Move lbDrive.Left + lbDrive.Width + 5
    End Select
    cmdLock.Move lbFiles.Left + lbFiles.Width - cmdLock.Width
    fraTagOptions.Move lbFiles.Left + lbFiles.Width + 5
    frmMain.Width = Screen.TwipsPerPixelX * (fraTagOptions.Left + fraTagOptions.Width + 10)
End Sub

Private Sub mnuViewMemory_Click()
    frmMemTag.Show vbModal
    MemTagToolTip
End Sub

Private Sub SaveFile(sFilePath As String)
    Dim lFileNumber&, lFileSize&
    Dim lRetVal&
    Dim tTag As mp3Tag
    
    If Not cmdSave.Enabled Then Exit Sub
    If Not bDirty Then Exit Sub
    
    If Dir$(sFilePath) = "" Then
        MsgBox "File """ & Get_FileTitle(sOldPath) & """ no longer exists. It may have been deleted or moved.", vbOKOnly + vbCritical, "Error."
        lbFiles.ListIndex = -1 'Launches lbfile_click
        Exit Sub
    End If
    
On Error GoTo FileErr
    If bHasTag Then
        With tTag
        .sTag = "TAG"
        .sAlbum = txtAlbum
        .sArtist = txtArtist
        .sComment = txtComment
        .sTrack = txtTrack
        .sTitle = txtTitle
        .sYear = txtYear
        .bGenre = CByte(cmbGenre.ItemData(cmbGenre.ListIndex))
        End With
        lRetVal = File_SaveTag(sFilePath, tTag, bOriginalTag)
        If lRetVal Then Err.Raise lRetVal
    Else
        If bOriginalTag Then File_Truncate sFilePath
    End If
    Exit Sub
FileErr:
    Reset
    ErrHandler Err
End Sub

Public Sub Tray(ByVal bWhat As Boolean, Optional ByVal bShow As Boolean = True)
    If bWhat Then
        VBGTray.uFlags = NIF_ICON Or NIF_TIP Or NIF_MESSAGE
        VBGTray.ucallbackMessage = WM_MOUSEMOVE
        VBGTray.hIcon = Icon
        VBGTray.szTip = Caption & vbNullChar
        Shell_NotifyIcon NIM_ADD, VBGTray
        App.TaskVisible = False
        Me.Hide
    Else
        Shell_NotifyIcon NIM_DELETE, VBGTray
        App.TaskVisible = True
        Me.WindowState = vbNormal
        If bShow Then Me.Show
        SetForegroundWindow hwnd
    End If
End Sub

Private Sub txtAlbum_Change()
    bDirty = True
End Sub

Private Sub txtArtist_Change()
    bDirty = True
End Sub

Private Sub txtComment_Change()
    bDirty = True
End Sub

Private Sub txtTitle_Change()
    bDirty = True
End Sub

Private Sub txtYear_Change()
    Dim dummy&
    bDirty = True
On Error GoTo ErrCLng
    dummy = CLng(txtYear)
    Exit Sub
ErrCLng:
    txtYear = ""
End Sub
