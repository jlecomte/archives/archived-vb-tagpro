VERSION 5.00
Begin VB.Form frmAbout 
   AutoRedraw      =   -1  'True
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "About MyApp"
   ClientHeight    =   2115
   ClientLeft      =   2340
   ClientTop       =   1935
   ClientWidth     =   5730
   ClipControls    =   0   'False
   Icon            =   "frmAbout.frx":0000
   LinkTopic       =   "Form2"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   141
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   382
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.TextBox txtDisclaimer 
      BackColor       =   &H8000000F&
      BorderStyle     =   0  'None
      Height          =   855
      Left            =   120
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   5
      TabStop         =   0   'False
      Text            =   "frmAbout.frx":0A02
      Top             =   1200
      Width           =   4095
   End
   Begin VB.PictureBox picTmp 
      AutoRedraw      =   -1  'True
      AutoSize        =   -1  'True
      BorderStyle     =   0  'None
      ClipControls    =   0   'False
      Height          =   225
      Left            =   5280
      ScaleHeight     =   15
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   20
      TabIndex        =   4
      Top             =   120
      Visible         =   0   'False
      Width           =   300
   End
   Begin VB.CommandButton cmdOK 
      Cancel          =   -1  'True
      Caption         =   "OK"
      Default         =   -1  'True
      Height          =   345
      Left            =   4320
      TabIndex        =   0
      Top             =   1680
      Width           =   1260
   End
   Begin VB.Label lblEmail 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      Caption         =   "Mail me"
      ForeColor       =   &H00FF0000&
      Height          =   255
      Left            =   4200
      TabIndex        =   3
      Top             =   1200
      Width           =   1455
   End
   Begin VB.Label lblLink 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      Caption         =   "Link to the site"
      ForeColor       =   &H00FF0000&
      Height          =   255
      Left            =   4200
      TabIndex        =   2
      Top             =   1440
      Width           =   1455
   End
   Begin VB.Line Line1 
      BorderColor     =   &H00808080&
      BorderStyle     =   6  'Inside Solid
      Index           =   1
      X1              =   6
      X2              =   376.933
      Y1              =   72
      Y2              =   72
   End
   Begin VB.Label lblTitle 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Application Title"
      ForeColor       =   &H00000000&
      Height          =   195
      Left            =   1080
      TabIndex        =   1
      Top             =   120
      Width           =   1125
   End
   Begin VB.Line Line1 
      BorderColor     =   &H00FFFFFF&
      BorderWidth     =   2
      Index           =   0
      X1              =   7
      X2              =   376.933
      Y1              =   73
      Y2              =   73
   End
End
Attribute VB_Name = "frmAbout"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
'  Copyright (C) 2000-2004, Julien Lecomte
'
'  This software Is provided 'as-is', without any express or implied
'  warranty.  In no event will the authors be held liable for any damages
'  arising from the use of this software.
'
'  Permission is granted to anyone to use this software for any purpose,
'  including commercial applications, and to alter it and redistribute it
'  freely, subject to the following restrictions:
'
'  1. The origin of this software must not be misrepresented; you must not
'     claim that you wrote the original software. If you use this software
'     in a product, an acknowledgment in the product documentation would be
'     appreciated but is not required.
'  2. Altered source versions must be plainly marked as such, and must not be
'     misrepresented as being the original software.
'  3. This notice may not be removed or altered from any source distribution.
'
Private Declare Function BitBlt Lib "gdi32" (ByVal hDestDC As Long, ByVal X As Long, ByVal Y As Long, ByVal nWidth As Long, ByVal nHeight As Long, ByVal hSrcDC As Long, ByVal xSrc As Long, ByVal ySrc As Long, ByVal dwRop As Long) As Long

Private Sub lblEmail_Click()
    ShellExecute HWND_TOP, vbNullString, "mailto:webmaster@example.org", vbNullString, vbNullString, vbNormalFocus
End Sub

Private Sub lblLink_Click()
    ShellExecute HWND_TOP, vbNullString, "http://www.example.org", vbNullString, vbNullString, vbNormalFocus
End Sub

Private Sub cmdOk_Click()
  Unload Me
End Sub

Private Sub Form_Load()
    Caption = "About " & App.Title
    lblTitle.Caption = App.Title & vbCrLf & "Version " & App.Major & "." & App.Minor & "." & App.Revision & _
                          "� 2000-2004, Julien Lecomte."
    txtDisclaimer = "This program is now open source software as of December 2004. Please refer to ""License.txt"" for additional information." & _
                    vbCrLf & vbCrLf & "Please download only free (legal) mp3's and mp3's you own."
    
    
    picTmp.Picture = LoadResPicture("LOGO_m", vbResBitmap)
    BitBlt hDC, 0&, 0&, picTmp.Width, picTmp.Height, picTmp.hDC, 0&, 0&, vbSrcAnd
    picTmp.Picture = LoadResPicture("LOGO", vbResBitmap)
    BitBlt hDC, 0&, 0&, picTmp.Width, picTmp.Height, picTmp.hDC, 0&, 0&, vbSrcPaint
    Refresh
    picTmp.AutoRedraw = False
End Sub
