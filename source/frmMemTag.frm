VERSION 5.00
Begin VB.Form frmMemTag 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Memory tag"
   ClientHeight    =   2340
   ClientLeft      =   45
   ClientTop       =   285
   ClientWidth     =   3570
   ControlBox      =   0   'False
   Icon            =   "frmMemTag.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   Moveable        =   0   'False
   ScaleHeight     =   2340
   ScaleWidth      =   3570
   StartUpPosition =   1  'CenterOwner
   Begin VB.CommandButton cmdCancel 
      Cancel          =   -1  'True
      Caption         =   "&Cancel"
      Height          =   375
      Left            =   1200
      TabIndex        =   1
      Top             =   1920
      Width           =   1095
   End
   Begin VB.TextBox txtTitle 
      Height          =   285
      Left            =   885
      MaxLength       =   30
      TabIndex        =   2
      Top             =   120
      Width           =   2655
   End
   Begin VB.TextBox txtYear 
      Height          =   285
      Left            =   885
      MaxLength       =   4
      TabIndex        =   5
      Top             =   1200
      Width           =   495
   End
   Begin VB.TextBox txtArtist 
      Height          =   285
      Left            =   885
      MaxLength       =   30
      TabIndex        =   3
      Top             =   480
      Width           =   2655
   End
   Begin VB.TextBox txtAlbum 
      Height          =   285
      Left            =   885
      MaxLength       =   30
      TabIndex        =   4
      Top             =   840
      Width           =   2655
   End
   Begin VB.TextBox txtComment 
      Height          =   285
      Left            =   885
      MaxLength       =   30
      TabIndex        =   7
      Top             =   1560
      Width           =   2655
   End
   Begin VB.ComboBox cmbGenre 
      Height          =   315
      Left            =   1920
      Sorted          =   -1  'True
      Style           =   2  'Dropdown List
      TabIndex        =   6
      Top             =   1200
      Width           =   1575
   End
   Begin VB.CommandButton cmdOk 
      Caption         =   "&OK"
      Default         =   -1  'True
      Height          =   375
      Left            =   2400
      TabIndex        =   0
      Top             =   1920
      Width           =   1095
   End
   Begin VB.Label lblTitle 
      Alignment       =   1  'Right Justify
      AutoSize        =   -1  'True
      Caption         =   "Title"
      Height          =   195
      Left            =   480
      TabIndex        =   13
      Top             =   120
      UseMnemonic     =   0   'False
      Width           =   300
   End
   Begin VB.Label lblArtist 
      Alignment       =   1  'Right Justify
      AutoSize        =   -1  'True
      Caption         =   "Artist"
      Height          =   195
      Left            =   435
      TabIndex        =   12
      Top             =   480
      UseMnemonic     =   0   'False
      Width           =   345
   End
   Begin VB.Label lblAlbum 
      Alignment       =   1  'Right Justify
      AutoSize        =   -1  'True
      Caption         =   "Album"
      Height          =   195
      Left            =   345
      TabIndex        =   11
      Top             =   840
      UseMnemonic     =   0   'False
      Width           =   435
   End
   Begin VB.Label lblYear 
      Alignment       =   1  'Right Justify
      AutoSize        =   -1  'True
      Caption         =   "Year"
      Height          =   195
      Left            =   450
      TabIndex        =   10
      Top             =   1200
      UseMnemonic     =   0   'False
      Width           =   330
   End
   Begin VB.Label lblComment 
      Alignment       =   1  'Right Justify
      AutoSize        =   -1  'True
      Caption         =   "Comment"
      Height          =   195
      Left            =   120
      TabIndex        =   9
      Top             =   1560
      UseMnemonic     =   0   'False
      Width           =   660
   End
   Begin VB.Label lblGenre 
      Alignment       =   1  'Right Justify
      AutoSize        =   -1  'True
      Caption         =   "Genre"
      Height          =   195
      Left            =   1440
      TabIndex        =   8
      Top             =   1200
      UseMnemonic     =   0   'False
      Width           =   435
   End
End
Attribute VB_Name = "frmMemTag"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
'  Copyright (C) 2000-2004, Julien Lecomte
'
'  This software Is provided 'as-is', without any express or implied
'  warranty.  In no event will the authors be held liable for any damages
'  arising from the use of this software.
'
'  Permission is granted to anyone to use this software for any purpose,
'  including commercial applications, and to alter it and redistribute it
'  freely, subject to the following restrictions:
'
'  1. The origin of this software must not be misrepresented; you must not
'     claim that you wrote the original software. If you use this software
'     in a product, an acknowledgment in the product documentation would be
'     appreciated but is not required.
'  2. Altered source versions must be plainly marked as such, and must not be
'     misrepresented as being the original software.
'  3. This notice may not be removed or altered from any source distribution.
'
Private Sub cmdCancel_Click()
    Unload Me
End Sub

Private Sub cmdOk_Click()
    tMemTag.sTag = "TAG"
    tMemTag.bGenre = cmbGenre.ListIndex
    tMemTag.sAlbum = txtAlbum
    tMemTag.sArtist = txtArtist
    tMemTag.sComment = txtComment
    tMemTag.sTitle = txtTitle
    tMemTag.sYear = txtYear
    Unload Me
End Sub

Private Sub Form_Load()
    Dim I&
    cmbGenre.AddItem ""
    cmbGenre.ItemData(cmbGenre.NewIndex) = 255
    For I = 1 To saGenreLimit
        cmbGenre.AddItem saGenres(I)
        cmbGenre.ItemData(cmbGenre.NewIndex) = I
    Next
    cmbGenre.ListIndex = 0
    
    If tMemTag.sTag = "TAG" Then
        txtAlbum = Trim$(tMemTag.sAlbum)
        txtArtist = Trim$(tMemTag.sArtist)
        txtComment = Trim$(tMemTag.sComment)
        txtTitle = Trim$(tMemTag.sTitle)
        txtYear = Trim$(tMemTag.sYear)
        cmbGenre.ListIndex = tMemTag.bGenre
    End If
End Sub
