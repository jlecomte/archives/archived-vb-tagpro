VERSION 5.00
Begin VB.Form frmBatchDirectory 
   Caption         =   "Batch directory"
   ClientHeight    =   3915
   ClientLeft      =   60
   ClientTop       =   300
   ClientWidth     =   3810
   ControlBox      =   0   'False
   Icon            =   "frmBatchDirectory.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   3915
   ScaleWidth      =   3810
   StartUpPosition =   1  'CenterOwner
   Begin VB.CommandButton cmdOk 
      Caption         =   "&OK"
      Default         =   -1  'True
      Height          =   375
      Left            =   2640
      TabIndex        =   1
      Top             =   3480
      Width           =   1095
   End
   Begin VB.CommandButton cmdCancel 
      Caption         =   "&Cancel"
      Height          =   375
      Left            =   1440
      TabIndex        =   0
      Top             =   3480
      Width           =   1095
   End
   Begin VB.CheckBox chkDelete 
      Caption         =   "Delete tag"
      Height          =   255
      Left            =   120
      TabIndex        =   6
      Top             =   480
      Width           =   3615
   End
   Begin VB.Frame fraAlbum 
      Caption         =   "Apply from memory tag :"
      Height          =   2415
      Left            =   120
      TabIndex        =   5
      Top             =   960
      Width           =   3615
      Begin VB.CommandButton cmdMemoryTag 
         Height          =   435
         Left            =   120
         MaskColor       =   &H00FF00FF&
         Picture         =   "frmBatchDirectory.frx":000C
         Style           =   1  'Graphical
         TabIndex        =   19
         ToolTipText     =   "View memory tag"
         Top             =   240
         UseMaskColor    =   -1  'True
         Width           =   495
      End
      Begin VB.ComboBox cmbOption 
         Height          =   315
         Index           =   0
         IntegralHeight  =   0   'False
         Left            =   1560
         Style           =   2  'Dropdown List
         TabIndex        =   7
         Top             =   240
         Width           =   1935
      End
      Begin VB.ComboBox cmbOption 
         Height          =   315
         Index           =   1
         IntegralHeight  =   0   'False
         Left            =   1560
         Style           =   2  'Dropdown List
         TabIndex        =   8
         Top             =   600
         Width           =   1935
      End
      Begin VB.ComboBox cmbOption 
         Height          =   315
         Index           =   2
         IntegralHeight  =   0   'False
         Left            =   1560
         Style           =   2  'Dropdown List
         TabIndex        =   9
         Top             =   960
         Width           =   1935
      End
      Begin VB.ComboBox cmbOption 
         Height          =   315
         Index           =   3
         IntegralHeight  =   0   'False
         Left            =   1560
         Style           =   2  'Dropdown List
         TabIndex        =   10
         Top             =   1320
         Width           =   1935
      End
      Begin VB.ComboBox cmbOption 
         Height          =   315
         Index           =   4
         IntegralHeight  =   0   'False
         Left            =   1560
         Style           =   2  'Dropdown List
         TabIndex        =   11
         Top             =   1680
         Width           =   1935
      End
      Begin VB.ComboBox cmbOption 
         Height          =   315
         Index           =   5
         IntegralHeight  =   0   'False
         Left            =   1560
         Style           =   2  'Dropdown List
         TabIndex        =   12
         Top             =   2040
         Width           =   1935
      End
      Begin VB.Label lblApply 
         Alignment       =   1  'Right Justify
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Comments"
         Height          =   195
         Index           =   5
         Left            =   765
         TabIndex        =   18
         Top             =   2040
         Width           =   735
      End
      Begin VB.Label lblApply 
         Alignment       =   1  'Right Justify
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Genre"
         Height          =   195
         Index           =   4
         Left            =   1065
         TabIndex        =   17
         Top             =   1680
         Width           =   435
      End
      Begin VB.Label lblApply 
         Alignment       =   1  'Right Justify
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Year"
         Height          =   195
         Index           =   3
         Left            =   1170
         TabIndex        =   16
         Top             =   1320
         Width           =   330
      End
      Begin VB.Label lblApply 
         Alignment       =   1  'Right Justify
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Album"
         Height          =   195
         Index           =   2
         Left            =   1065
         TabIndex        =   15
         Top             =   960
         Width           =   435
      End
      Begin VB.Label lblApply 
         Alignment       =   1  'Right Justify
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Artist"
         Height          =   195
         Index           =   1
         Left            =   1155
         TabIndex        =   14
         Top             =   600
         Width           =   345
      End
      Begin VB.Label lblApply 
         Alignment       =   1  'Right Justify
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Title"
         Height          =   195
         Index           =   0
         Left            =   1200
         TabIndex        =   13
         Top             =   240
         Width           =   300
      End
   End
   Begin VB.CheckBox chkCreate 
      Caption         =   "Create artist and song name from title."
      Height          =   255
      Left            =   120
      TabIndex        =   4
      Top             =   720
      Value           =   1  'Checked
      Width           =   3615
   End
   Begin VB.TextBox txtLocation 
      BackColor       =   &H8000000F&
      Height          =   285
      Left            =   120
      Locked          =   -1  'True
      MaxLength       =   255
      TabIndex        =   3
      TabStop         =   0   'False
      Text            =   "C:\"
      Top             =   120
      Width           =   3255
   End
   Begin VB.CommandButton cmdBrowse 
      Caption         =   "..."
      Height          =   255
      Left            =   3480
      TabIndex        =   2
      Top             =   120
      Width           =   255
   End
End
Attribute VB_Name = "frmBatchDirectory"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
'  Copyright (C) 2000-2004, Julien Lecomte
'
'  This software Is provided 'as-is', without any express or implied
'  warranty.  In no event will the authors be held liable for any damages
'  arising from the use of this software.
'
'  Permission is granted to anyone to use this software for any purpose,
'  including commercial applications, and to alter it and redistribute it
'  freely, subject to the following restrictions:
'
'  1. The origin of this software must not be misrepresented; you must not
'     claim that you wrote the original software. If you use this software
'     in a product, an acknowledgment in the product documentation would be
'     appreciated but is not required.
'  2. Altered source versions must be plainly marked as such, and must not be
'     misrepresented as being the original software.
'  3. This notice may not be removed or altered from any source distribution.
'
Private Sub chkDelete_Click()
    Dim bVal As Boolean
    Dim I&
    Select Case chkDelete
        Case vbChecked
            bVal = False
        Case vbUnchecked
            bVal = True
    End Select
    chkCreate.Enabled = bVal
    For I = cmbOption.lBound To cmbOption.UBound
        cmbOption(I).Enabled = bVal
    Next I
End Sub

Private Sub chkExists_Click()

End Sub

Private Sub cmdBrowse_Click()
    Dim sFolder$
    sFolder = Shell_BrowseFolder(hwnd, "Please choose folder.")
    If sFolder = "" Then Exit Sub
    txtLocation = sFolder
End Sub

Private Sub cmdCancel_Click()
    Unload Me
End Sub

Private Sub cmdMemoryTag_Click()
    frmMemTag.Show vbModal
End Sub

Private Sub cmdOk_Click()
    If Create_Command Then Exit Sub
    
    SetRegistry HKEY_CURRENT_USER, sRegistryBatch, "Create", chkCreate.Value, REG_DWORD
    SetRegistry HKEY_CURRENT_USER, sRegistryBatch, "Delete", chkDelete.Value, REG_DWORD
    SetRegistry HKEY_CURRENT_USER, sRegistryBatch, "Title", cmbOption(0).ListIndex, REG_DWORD
    SetRegistry HKEY_CURRENT_USER, sRegistryBatch, "Artist", cmbOption(1).ListIndex, REG_DWORD
    SetRegistry HKEY_CURRENT_USER, sRegistryBatch, "Comment", cmbOption(5).ListIndex, REG_DWORD
    SetRegistry HKEY_CURRENT_USER, sRegistryBatch, "Album", cmbOption(2).ListIndex, REG_DWORD
    SetRegistry HKEY_CURRENT_USER, sRegistryBatch, "Year", cmbOption(3).ListIndex, REG_DWORD
    SetRegistry HKEY_CURRENT_USER, sRegistryBatch, "Genre", cmbOption(4).ListIndex, REG_DWORD
    
    Unload Me
End Sub

Private Function Create_Command() As Boolean
    Dim sCommand$
    Dim lRetVal&

    sCommand = "I/C """ & AddSlash(txtLocation) & "*.mp3"""
    If chkDelete Then
        sCommand = sCommand & " /-"
        GoTo Resolve
    End If
    sCommand = sCommand & IIf(chkCreate = vbChecked, " /M", "")
    sCommand = sCommand & " /" & cmbOption(0).ListIndex
    sCommand = sCommand & " """ & Trim$(tMemTag.sTitle) & """"
    sCommand = sCommand & " /" & cmbOption(1).ListIndex
    sCommand = sCommand & " """ & Trim$(tMemTag.sArtist) & """"
    sCommand = sCommand & " /" & cmbOption(2).ListIndex
    sCommand = sCommand & " """ & Trim$(tMemTag.sAlbum) & """"
    sCommand = sCommand & " /" & cmbOption(3).ListIndex
    sCommand = sCommand & " """ & Trim$(tMemTag.sYear) & """"
    sCommand = sCommand & " /" & cmbOption(5).ListIndex
    sCommand = sCommand & " """ & Trim$(tMemTag.sComment) & """"
    sCommand = sCommand & " /" & cmbOption(4).ListIndex
    sCommand = sCommand & " """ & tMemTag.bGenre & """"

Resolve:
    Create_Command = CBool(Command_Act(sCommand))
End Function

Private Sub Form_Load()
    Dim lRegVal&
    Dim I&
    For I = cmbOption.lBound To cmbOption.UBound
        cmbOption(I).AddItem "Don't apply"
        cmbOption(I).AddItem "Apply if one existed"
        cmbOption(I).AddItem "Apply if none existed"
        cmbOption(I).AddItem "Apply"
    Next I
    
    GetRegistry HKEY_CURRENT_USER, sRegistryBatch, "Create", lRegVal, vbChecked
    chkCreate.Value = lRegVal
    GetRegistry HKEY_CURRENT_USER, sRegistryBatch, "Delete", lRegVal, vbUnchecked
    chkDelete.Value = lRegVal
    GetRegistry HKEY_CURRENT_USER, sRegistryBatch, "Title", lRegVal, 3
    cmbOption(0).ListIndex = lRegVal
    GetRegistry HKEY_CURRENT_USER, sRegistryBatch, "Artist", lRegVal, 3
    cmbOption(1).ListIndex = lRegVal
    GetRegistry HKEY_CURRENT_USER, sRegistryBatch, "Comment", lRegVal, 0
    cmbOption(5).ListIndex = lRegVal
    GetRegistry HKEY_CURRENT_USER, sRegistryBatch, "Album", lRegVal, 3
    cmbOption(2).ListIndex = lRegVal
    GetRegistry HKEY_CURRENT_USER, sRegistryBatch, "Year", lRegVal, 3
    cmbOption(3).ListIndex = lRegVal
    GetRegistry HKEY_CURRENT_USER, sRegistryBatch, "Genre", lRegVal, 0
    cmbOption(4).ListIndex = lRegVal
End Sub

Private Sub txtLocation_Change()
    txtLocation.ToolTipText = txtLocation
End Sub
