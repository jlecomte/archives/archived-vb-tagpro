VERSION 5.00
Begin VB.Form frmMakeIndex 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Make index of location"
   ClientHeight    =   3315
   ClientLeft      =   45
   ClientTop       =   285
   ClientWidth     =   4290
   ControlBox      =   0   'False
   HasDC           =   0   'False
   Icon            =   "frmMakeIndex.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   Moveable        =   0   'False
   ScaleHeight     =   221
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   286
   StartUpPosition =   1  'CenterOwner
   Begin VB.CheckBox chkSaveAs 
      Caption         =   "Save as a .htm file."
      Height          =   195
      Left            =   120
      TabIndex        =   12
      Top             =   2640
      Width           =   4095
   End
   Begin VB.Frame fraFormat 
      Caption         =   "Format of index"
      Height          =   2055
      Left            =   120
      TabIndex        =   14
      Top             =   480
      Width           =   4095
      Begin VB.CommandButton cmdIndex 
         Caption         =   "File size"
         Height          =   375
         Index           =   7
         Left            =   1560
         TabIndex        =   11
         Top             =   1560
         Width           =   975
      End
      Begin VB.CommandButton cmdIndex 
         Caption         =   "Comment"
         Height          =   375
         Index           =   6
         Left            =   120
         TabIndex        =   10
         Top             =   1560
         Width           =   975
      End
      Begin VB.CommandButton cmdIndex 
         Cancel          =   -1  'True
         Caption         =   "Genre"
         Height          =   375
         Index           =   5
         Left            =   3000
         TabIndex        =   9
         Top             =   1080
         Width           =   975
      End
      Begin VB.CommandButton cmdIndex 
         Caption         =   "Year"
         Height          =   375
         Index           =   4
         Left            =   1560
         TabIndex        =   8
         Top             =   1080
         Width           =   975
      End
      Begin VB.CommandButton cmdIndex 
         Caption         =   "Album"
         Height          =   375
         Index           =   3
         Left            =   120
         TabIndex        =   7
         Top             =   1080
         Width           =   975
      End
      Begin VB.CommandButton cmdIndex 
         Caption         =   "Artist"
         Height          =   375
         Index           =   2
         Left            =   3000
         TabIndex        =   6
         Top             =   600
         Width           =   975
      End
      Begin VB.CommandButton cmdIndex 
         Caption         =   "Title"
         Height          =   375
         Index           =   1
         Left            =   1560
         TabIndex        =   5
         Top             =   600
         Width           =   975
      End
      Begin VB.CommandButton cmdIndex 
         Caption         =   "File name"
         Height          =   375
         Index           =   0
         Left            =   120
         TabIndex        =   4
         Top             =   600
         Width           =   975
      End
      Begin VB.TextBox txtFormat 
         Height          =   285
         Left            =   120
         MaxLength       =   255
         TabIndex        =   3
         Text            =   "%N% : %2%"
         Top             =   240
         Width           =   3855
      End
   End
   Begin VB.CommandButton cmdCancel 
      Caption         =   "&Cancel"
      Height          =   375
      Left            =   1920
      TabIndex        =   1
      Top             =   2880
      Width           =   1095
   End
   Begin VB.CommandButton cmdOk 
      Caption         =   "&OK"
      Default         =   -1  'True
      Height          =   375
      Left            =   3120
      TabIndex        =   0
      Top             =   2880
      Width           =   1095
   End
   Begin VB.CommandButton cmdBrowse 
      Caption         =   "..."
      Height          =   255
      Left            =   3960
      TabIndex        =   2
      Top             =   120
      Width           =   255
   End
   Begin VB.TextBox txtLocation 
      BackColor       =   &H8000000F&
      Height          =   285
      Left            =   120
      Locked          =   -1  'True
      MaxLength       =   255
      TabIndex        =   13
      TabStop         =   0   'False
      Text            =   "C:\"
      Top             =   120
      Width           =   3735
   End
   Begin VB.Menu mnuSizes 
      Caption         =   "Sizes"
      Visible         =   0   'False
      Begin VB.Menu mnuSize 
         Caption         =   "in bytes"
         Index           =   0
      End
      Begin VB.Menu mnuSize 
         Caption         =   "in kilobytes"
         Index           =   1
      End
      Begin VB.Menu mnuSize 
         Caption         =   "in megabytes"
         Index           =   2
      End
   End
End
Attribute VB_Name = "frmMakeIndex"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Option Compare Text
'  Copyright (C) 2000-2004, Julien Lecomte
'
'  This software Is provided 'as-is', without any express or implied
'  warranty.  In no event will the authors be held liable for any damages
'  arising from the use of this software.
'
'  Permission is granted to anyone to use this software for any purpose,
'  including commercial applications, and to alter it and redistribute it
'  freely, subject to the following restrictions:
'
'  1. The origin of this software must not be misrepresented; you must not
'     claim that you wrote the original software. If you use this software
'     in a product, an acknowledgment in the product documentation would be
'     appreciated but is not required.
'  2. Altered source versions must be plainly marked as such, and must not be
'     misrepresented as being the original software.
'  3. This notice may not be removed or altered from any source distribution.
'
Dim sArray(0 To 9) As String

Private Sub cmdBrowse_Click()
    Dim sFolder$
    sFolder = Shell_BrowseFolder(hwnd, "Please choose folder.")
    If sFolder = "" Then Exit Sub
    txtLocation = sFolder
End Sub

Private Sub cmdCancel_Click()
    Unload Me
End Sub

Private Sub cmdIndex_Click(Index As Integer)
    Select Case Index
        Case 7
            PopupMenu mnuSizes
        Case Else
            txtFormat = txtFormat & sArray(Index)
    End Select
End Sub

Private Sub cmdOk_Click()
    Dim I&
    Dim bOk As Boolean
    Dim sStr$
    Dim sFolderPath$
    Dim sFirstFile$
    
    sStr = txtFormat
    For I = 0 To 9
        If InStr(1, sStr, sArray(I)) Then
            bOk = True
            Exit For
        End If
    Next
    If Not bOk Then
        MsgBox "The format line has no arguments.", vbOKOnly + vbCritical, "Error"
        Exit Sub
    End If
    
    sFolderPath = txtLocation & IIf(Right$(txtLocation, 1) = "\", "", "\")
    sFirstFile = Dir$(sFolderPath & "*.mp3")
    If sFirstFile = "" Then
        MsgBox "There are no mp3 files in the folder!"
        Exit Sub
    End If
    
    MakeFileList sStr, sFolderPath, CBool(chkSaveAs.Value)
    SetRegistry HKEY_CURRENT_USER, sRegistryTagPro, "Index format", sStr, REG_SZ
    Unload Me
End Sub

Private Function EditLen(sSingle As Single) As String
    EditLen = Format$(sSingle, "###.##")
End Function

Private Sub Form_Load()
    Dim sFor$
    Dim lRetVal&
    lRetVal = GetRegistry(HKEY_CURRENT_USER, sRegistryTagPro, "Index format", sFor, REG_SZ)
    If lRetVal = REG_SZ Then
        txtFormat = sFor
    Else
        txtFormat = "%N% : %2% mb"
    End If
        
    sArray(0) = "%N%"
    sArray(1) = "%T%"
    sArray(2) = "%A%"
    sArray(3) = "%L%"
    sArray(4) = "%Y%"
    sArray(5) = "%G%"
    sArray(6) = "%C%"
    sArray(7) = "%0%"
    sArray(8) = "%1%"
    sArray(9) = "%2%"
End Sub

Private Sub MakeFileList(ByVal sFormat As String, sSearchPath As String, bHTML As Boolean)
    Dim lFile&
    Dim lMP3&
    Dim sOutput$
    Dim sFile$
    Dim sTmp$
    Dim sTmpFormat$
    Dim iCount&
    Dim mp3Count&
    Dim bNeedsToOpen As Boolean
    Dim I&, J&
    Dim MP As mp3Tag
    Dim Char As String * 1
    Dim sArg As String * 3

    ReDim aString(1 To 10) As String
    
    MousePointer = vbHourglass
    For I = 1 To 6
        If InStr(1, sFormat, sArray(I)) Then
            bNeedsToOpen = True
            Exit For
        End If
    Next
    
    iCount = 1
    ReDim sArguments(1 To iCount) As String
    ReDim bIsArg(1 To iCount) As Boolean
    
    sTmpFormat = sFormat + Space$(5)
    'Create Argument list
    For I = 1 To Len(sFormat)
        Char = Mid$(sTmpFormat, I, 1)
        If Char = "%" Then
            sArg = Mid$(sTmpFormat, I, 3)
            For J = 0 To 9
                If sArg = sArray(J) Then
                    iCount = iCount + 2
                    ReDim Preserve sArguments(1 To iCount) As String
                    ReDim Preserve bIsArg(1 To iCount) As Boolean
                    bIsArg(iCount - 1) = True
                    sArguments(iCount - 1) = sArg
                    I = I + 2
                    Exit For
                End If
            Next
        Else
            sArguments(iCount) = sArguments(iCount) & Char
        End If
    Next
    
On Error GoTo ErrorHandle
    'Open & close for text
    sOutput = sSearchPath & "mp3index." & IIf(bHTML, "htm", "txt")
    lFile = FreeFile
    Open sOutput For Output Access Write As lFile
    
    'Create array
    sFile = Dir$(sSearchPath & "*.mp3")
    iCount = 0
    Do While sFile <> ""
        mp3Count = mp3Count + 1
        iCount = (iCount + 1)
        If iCount = 10 Then
            iCount = iCount Mod 10
            ReDim Preserve aString(1 To UBound(aString) + 10) As String
        End If
        aString(mp3Count) = sFile
        sFile = Dir
    Loop
    ReDim Preserve aString(1 To mp3Count) As String
    
    'Sort the array in alphabetical order.
    For I = 1 To mp3Count
        For J = 1 To mp3Count - 1
            If aString(I) < aString(J) Then
                sTmp = aString(J)
                aString(J) = aString(I)
                aString(I) = sTmp
            End If
        Next
    Next
    
    'Stylize it
    Select Case bHTML
        Case False
            Print #lFile, "mp3 Index",
            Print #lFile, "Generated by TagPro"
            Print #lFile, Date$ & " " & Time$
            Print #lFile, ""
            Print #lFile, mp3Count & " files found."
            Print #lFile, ""
        Case True
            Print #lFile, "<html><head>"
            Print #lFile, "<link rel=""stylesheet"" href=""null"">"
            Print #lFile, "<style TYPE=""text/css""><!--BODY { background: #000040; }"
            Print #lFile, ".para1 { margin-top: -42px; margin-left: 145px; margin-right: 10px; font-family: ""font2, Arial""; font-size: 30px; line-height: 35px; text-align: left; color: #E1E1E1; }"
            Print #lFile, ".para2 { margin-top: 15px; margin-left: 15px; margin-right: 50px; font-family: ""font1, Arial Black""; font-size: 50px; line-height: 40px; text-align: left; color: #004080; }"
            Print #lFile, "--></style><title>TagPro Generated Index</title></head>"
            Print #lFile, "<body BGCOLOR=""#000080"" topmargin=""0"" leftmargin=""0"" text=""#FFFFFF"" link=""#FF0000"" vlink=""#FF0000"">"
            Print #lFile, "<div align=""center""><div CLASS=""para2"" Align = ""center"">"
            Print #lFile, "<p>TagPro</p></div><div CLASS=""para1"" align=""center"">"
            Print #lFile, "<p>Index</p></div></div>"
            Print #lFile, "<hr align=""left"" width=""90%"" noshade size=""1"" color=""#FFBF00"">"
            Print #lFile, "<div align=""right"">"
            Print #lFile, "<table border=""0"" cellspacing=""0"" cellpadding=""0"" width=""98%"">"
            Print #lFile, "<tr><td><small><small><font color=""#409FFF"" face=""Arial"">"
            Print #lFile, "Index of </font><font face=""Arial"" color=""#FFBF00"">"
            Print #lFile, txtLocation
            Print #lFile, "</font></small></small><br><small><small><font face=""Arial"" color=""#FFBF00"">"
            Print #lFile, mp3Count
            Print #lFile, "</font><font color=""#409FFF"" face=""Arial""> files found</font></small></small><br>"
            Print #lFile, "<font color=""#409FFF"" face=""Arial""><small><small>Generated by <a href=""http://thunder.prohosting.com/~jagsite"">TagPro</a></small></small></font></td></tr></table></div>"
            Print #lFile, "<blockquote><p><big><font face=""Arial"" color=""#FFBF00"">Files:</font></big></p><blockquote><p>"
    End Select
    
    For I = 1 To mp3Count
        sFile = sSearchPath & aString(I)
        If bNeedsToOpen Then
            lMP3 = FreeFile
            Open sFile For Binary Access Read As lMP3
            Get lMP3, LOF(lMP3) - Len(MP) + 1, MP
            Close lMP3
        End If
        'Analyze string & write in detail
        sTmp = ""
        For J = 1 To UBound(sArguments)
            If bIsArg(J) Then
                Select Case sArguments(J)
                    Case sArray(0)
                        sTmp = sTmp & Left$(aString(I), Len(aString(I)) - 4)
                    Case sArray(1)
                        If MP.sTag = "TAG" Then sTmp = sTmp & Trim$(MP.sTitle)
                    Case sArray(2)
                        If MP.sTag = "TAG" Then sTmp = sTmp & Trim$(MP.sArtist)
                    Case sArray(3)
                        If MP.sTag = "TAG" Then sTmp = sTmp & Trim$(MP.sAlbum)
                    Case sArray(4)
                        If MP.sTag = "TAG" Then sTmp = sTmp & Trim$(MP.sYear)
                    Case sArray(5)
                        If MP.sTag = "TAG" Then sTmp = sTmp & saGenres(MP.bGenre)
                    Case sArray(6)
                        If MP.sTag = "TAG" Then sTmp = sTmp & Trim$(MP.sComment)
                    Case sArray(7)
                        sTmp = sTmp & EditLen(FileLen(sFile))
                    Case sArray(8)
                        sTmp = sTmp & EditLen(FileLen(sFile) / 1024)
                    Case sArray(9)
                        sTmp = sTmp & EditLen(FileLen(sFile) / (1024 ^ 2))
                End Select
            Else
                sTmp = sTmp & sArguments(J)
            End If
        Next
        If bHTML Then sTmp = sTmp & "<br>"
        Print #lFile, sTmp
    Next
    
    If bHTML Then Print #lFile, "</p></blockquote></blockquote></body></html>"
    
    Close lFile
    
    MousePointer = vbNormal
    ShellExecute HWND_TOP, vbNullString, sOutput, vbNullString, vbNullString, vbNormalFocus
    Exit Sub
ErrorHandle:
    MousePointer = vbNormal
    Reset
    ErrHandler Err
End Sub

Private Sub mnuSize_Click(Index As Integer)
    txtFormat = txtFormat & sArray(7 + Index)
End Sub

Private Sub txtLocation_Change()
    txtLocation.ToolTipText = txtLocation
End Sub
